package fr.epita.cri.maas.cli.commands;

import java.util.LinkedList;
import java.util.List;

public class WorkerCommand extends Command {

    @Override
    public void handle(LinkedList<String> cmdPartsQueue) {
        switch (cmdPartsQueue.removeFirst()) {
            case "list":
                listWorkers();
                break;
            case "add":
                addWorker(cmdPartsQueue.removeFirst());
                break;
            case "delete":
                deleteWorker(cmdPartsQueue.removeFirst());
                break;
        }
    }

    private void listWorkers() {
        List<String> tenantsNames = restApi.get("/worker", List.class);
        System.out.println("Workers: ");
        System.out.println(String.join(", ", tenantsNames));
    }

    private void addWorker(String name) {
        String token = restApi.post("/worker", String.class, name);
        System.out.println("Runner auth token created : " + token);
    }

    private void deleteWorker(String name) {
        restApi.delete("/worker/" + name, null);
        System.out.println("Runner deleted");
    }
}
