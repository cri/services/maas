package fr.epita.cri.maas.cli.commands;

import java.util.LinkedList;
import java.util.List;

public class TenantCommand extends Command {

    @Override
    public void handle(LinkedList<String> cmdPartsQueue) {
        switch (cmdPartsQueue.removeFirst()) {
            case "list":
                listTenants();
                break;
            case "add":
                String imageRegex = cmdPartsQueue.removeFirst();
                addTenant(imageRegex);
                break;
            case "delete":
                deleteTenant();
                break;
        }
    }

    private void listTenants() {
        List<String> tenantsNames = restApi.get("/tenant", List.class);
        System.out.println("Tenants: ");
        System.out.println(String.join(", ", tenantsNames));
    }

    private void addTenant(String imageRegex) {
        restApi.put("/tenant", String.class, imageRegex);
        System.out.println("Tenant added");
    }

    private void deleteTenant() {
        restApi.delete("/tenant", List.class);
        System.out.println("Tenant deleted");
    }
}
