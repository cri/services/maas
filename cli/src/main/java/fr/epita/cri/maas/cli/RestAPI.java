package fr.epita.cri.maas.cli;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.epita.cri.maas.util.FileUtils;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@RequiredArgsConstructor
public class RestAPI {
    private static final HttpClient client = HttpClient.newBuilder().version(HttpClient.Version.HTTP_2).build();
    private static final ObjectMapper objectMapper = new ObjectMapper();
    @Getter private final String remoteUrl;
    private final HashMap<String, String> authorizationHeaders;

    public <T> T get(String path, Class<T> clazz) {
        return runRequest(clazz, path, HttpRequest.Builder::GET);
    }

    public <T> T put(String path, Class<T> clazz, String body) {
        return runRequest(clazz, path, b -> b.PUT(HttpRequest.BodyPublishers.ofString(body)));
    }

    public <T> T post(String path, Class<T> clazz, String body) {
        return runRequest(clazz, path, b -> b.POST(HttpRequest.BodyPublishers.ofString(body)));
    }

    public <T> T delete(String path, Class<T> clazz) {
        return runRequest(clazz, path, HttpRequest.Builder::DELETE);
    }

    private <T> T runRequest(Class<T> clazz, String path, Function<HttpRequest.Builder, HttpRequest.Builder> methodConsumer) {
        URI uri = URI.create(remoteUrl + path);
        try {
            HttpRequest.Builder builder = addAuthorization(HttpRequest.newBuilder(uri), authorizationHeaders);
            builder = methodConsumer.apply(builder);
            builder = builder.header("Content-Type", "application/json");
            HttpRequest request = builder.build();
            HttpResponse<String> requestResult = client.send(request, HttpResponse.BodyHandlers.ofString());
            if (requestResult.statusCode() == 200) {
                if (clazz == String.class) {
                    return (T) requestResult.body();
                } else if (clazz == null) {
                    return null;
                }
                return objectMapper.readValue(requestResult.body(), clazz);
            } else {
                System.out.println("Unexpected result (" + request.method() + ":" + requestResult.statusCode() + ") " + uri);
                System.exit(1);
            }
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        }
        System.out.println("Unexpected result " + uri);
        System.exit(1);
        return null;
    }

    private static HttpRequest.Builder addAuthorization(HttpRequest.Builder builder, HashMap<String, String> authorizationHeaders) {
        for (Map.Entry<String, String> entry : authorizationHeaders.entrySet()) {
            builder = builder.header(entry.getKey(), entry.getValue());
        }
        return builder;
    }

    public void downloadFile(String url, File outputFile) {
        FileUtils.downloadFile(remoteUrl + url, outputFile, authorizationHeaders);
    }
}
