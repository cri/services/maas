package fr.epita.cri.maas.cli;

import com.oracle.svm.core.annotate.AutomaticFeature;
import fr.epita.cri.maas.data.workflow.Workflow;
import fr.epita.cri.maas.data.workflow.WorkflowDeserializer;
import fr.epita.cri.maas.data.workflow.WorkflowStage;
import fr.epita.cri.maas.data.workflow.WorkflowTask;
import fr.epita.cri.maas.rest.CreatePipelineData;
import org.graalvm.nativeimage.hosted.Feature;
import org.graalvm.nativeimage.hosted.RuntimeReflection;

@AutomaticFeature
class RuntimeReflectionRegistrationFeature implements Feature {
    public void beforeAnalysis(Feature.BeforeAnalysisAccess access) {
        registerDataClass(CreatePipelineData.class);

        registerDataClass(Workflow.class);
        registerDataClass(WorkflowDeserializer.class);
        registerDataClass(WorkflowStage.class);
        registerDataClass(WorkflowTask.class);
    }

    public void registerDataClass(Class<?> clazz) {
        RuntimeReflection.registerForReflectiveInstantiation(clazz);
        RuntimeReflection.register(clazz);
        RuntimeReflection.register(clazz.getDeclaredFields());
        RuntimeReflection.register(clazz.getMethods());
    }
}
