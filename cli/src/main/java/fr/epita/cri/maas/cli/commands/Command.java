package fr.epita.cri.maas.cli.commands;

import fr.epita.cri.maas.cli.RestAPI;

import java.util.LinkedList;

public abstract class Command {
    protected RestAPI restApi;

    public void init(RestAPI restApi) {
        this.restApi = restApi;
    }

    public abstract void handle(LinkedList<String> cmdPartsQueue);
}
