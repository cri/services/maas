package fr.epita.cri.maas.cli.commands;

import java.util.LinkedList;
import java.util.UUID;

public class TokenCommand extends Command {

    @Override
    public void handle(LinkedList<String> cmdPartsQueue) {
        switch (cmdPartsQueue.removeFirst()) {
            case "list":
                listTokens();
                break;
            case "add":
                addToken(String.join(" ", cmdPartsQueue));
                break;
            case "delete":
                UUID token = UUID.fromString(cmdPartsQueue.removeFirst());
                deleteToken(token);
                break;
        }
    }

    private void listTokens() {
        //TODO List tokens without leaking the token...
    }

    private void addToken(String comment) {
        UUID token = restApi.post("/tenant/apitoken", UUID.class, comment);
        System.out.println("Token created : " + token);
    }

    private void deleteToken(UUID token) {
        restApi.delete("/tenant/apitoken/" + token, null);
        System.out.println("Token deleted");
    }
}
