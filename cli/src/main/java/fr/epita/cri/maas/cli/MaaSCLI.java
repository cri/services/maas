package fr.epita.cri.maas.cli;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.epita.cri.maas.cli.commands.Command;
import fr.epita.cri.maas.cli.commands.WorkerCommand;
import fr.epita.cri.maas.cli.commands.TenantCommand;
import fr.epita.cri.maas.cli.commands.TokenCommand;
import fr.epita.cri.maas.data.pipeline.*;
import fr.epita.cri.maas.data.workflow.Workflow;
import fr.epita.cri.maas.data.workflow.WorkflowDeserializer;
import fr.epita.cri.maas.rest.CreatePipelineData;
import fr.epita.cri.maas.runner.PipelineRunner;
import fr.epita.cri.maas.util.FileUtils;
import fr.epita.cri.maas.util.SerializerUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class MaaSCLI {
    private static RestAPI restAPI;

    public static void main(String[] args) {
        LinkedList<String> argsQueue = new LinkedList<>(Arrays.asList(args));

        String remoteUrl = null;
        String sourceDataUrl = null;
        String inputFilePath = null;
        String remoteUserToken = null;
        String remoteTenant = null;
        String remoteApiToken = null;
        HashMap<String, String> environmentVariables = new HashMap<>();

        LinkedList<String> cmdPartsQueue = new LinkedList<>();
        try {
            while (!argsQueue.isEmpty()) {
                String value;
                switch (value = argsQueue.removeFirst()) {
                    case "--help":
                        printUsageAndExit();
                        break;
                    case "--in":
                    case "--input-file":
                        inputFilePath = argsQueue.removeFirst();
                        break;
                    case "-e":
                    case "--env":
                        String[] argsPart = argsQueue.removeFirst().split("=");
                        if (argsPart.length < 2) {
                            printUsageAndExit();
                        }
                        environmentVariables.put(argsPart[0], argsPart[1]);
                        break;
                    case "--remote":
                        remoteUrl = argsQueue.removeFirst();
                        break;
                    case "--remote-user-token":
                        remoteUserToken = argsQueue.removeFirst();
                        break;
                    case "--remote-tenant":
                        remoteTenant = argsQueue.removeFirst();
                        break;
                    case "--remote-api-token":
                        remoteApiToken = argsQueue.removeFirst();
                        break;
                    case "--in-url":
                        sourceDataUrl = argsQueue.removeFirst();
                        break;
                    default:
                        cmdPartsQueue.add(value);
                }
            }
        } catch (NoSuchElementException ignore) {
            printUsageAndExit();
        }

        if (remoteUrl != null) {
            if (remoteUserToken != null && remoteTenant == null) {
                printErrorAndExit("You need to specify the tenant name when authenticating with a user token (eg: --remote-tenant main)");
            } else if (remoteApiToken != null && remoteUserToken != null) {
                printErrorAndExit("You need to choose between user token and api token for remote authentication");
            } else if (remoteUserToken == null && remoteApiToken == null) {
                printErrorAndExit("You need to specify a user token or api token for remote authentication");
            }

            HashMap<String, String> authorizationHeaders = getAuthorizationHeaders(remoteUserToken, remoteTenant, remoteApiToken);
            restAPI = new RestAPI(remoteUrl, authorizationHeaders);
        }


        if (cmdPartsQueue.isEmpty()) {
            System.err.println("Missing command, don't know what to do");
            printUsageAndExit();
        }
        String commandToken = cmdPartsQueue.removeFirst();

        HashMap<String, Command> commands = new HashMap();
        commands.put("tenant", new TenantCommand());
        commands.put("apitoken", new TokenCommand());
        commands.put("worker", new WorkerCommand());

        commands.values().forEach(c -> c.init(restAPI));

        switch (commandToken) {
            case "run":
                if (cmdPartsQueue.isEmpty()) {
                    printErrorAndExit("Missing workflow path");
                }
                String workflowPath = cmdPartsQueue.removeFirst();
                runWorkflow(sourceDataUrl, inputFilePath, environmentVariables, workflowPath);
                break;
            default:
                Command command = commands.get(commandToken);
                if (command == null) {
                    printErrorAndExit("Unable to find command " + commandToken);
                }
                command.handle(cmdPartsQueue);
        }
    }

    private static void runWorkflow(String sourceDataUrl, String inputFilePath, HashMap<String, String> environmentVariables, String workflowPath) {
        String workflowFileContent = getWorkflowFileContent(workflowPath);
        Workflow workflow = WorkflowDeserializer.getWorkflow(workflowFileContent);
        if (workflow == null) {
            printErrorAndExit("Unable to read workflow");
            return;
        }

        byte[] inputData = readInputData(inputFilePath);

        File output = new File("output");
        workflow.sanitize();

        if (restAPI == null) {
            System.out.println("Using local runner");
            runWorkflowLocally(environmentVariables, workflow, inputData, output);
        } else {
            System.out.println("Using remote runner " + restAPI.getRemoteUrl());
            ObjectMapper jsonObjectMapper = SerializerUtils.getJsonObjectMapper();
            CreatePipelineData createPipelineData = new CreatePipelineData(workflowFileContent,
                    environmentVariables,
                    sourceDataUrl,
                    inputData,
                    null);
            try {
                UUID pipelineUuid = restAPI.post("/pipeline/", UUID.class, jsonObjectMapper.writeValueAsString(createPipelineData));
                System.out.println("Remote workflow UUID: " + pipelineUuid);

                int lastLogsLinesCount = 0;
                while (true) {
                    Thread.sleep(5000);
                    boolean shouldStop = false;
                    PipelineStatus pipelineStatus = restAPI.get("/pipeline/" + pipelineUuid, PipelineStatus.class);
                    if (pipelineStatus.isDone()) {
                        shouldStop = true;
                    }

                    String getLogsLogs = restAPI.get("/pipeline/" + pipelineUuid + "/logs", String.class);
                    String[] lines = getLogsLogs.split("\r\n");
                    for (int i = lastLogsLinesCount; i < lines.length; i++) {
                        System.out.println(lines[i]);
                    }
                    lastLogsLinesCount = lines.length;

                    if (shouldStop) {
                        break;
                    }
                }

                //Download result file
                FileUtils.deleteDir(output);
                output.mkdirs();
                File tempZipFile = File.createTempFile("maas", ".tmp");
                restAPI.downloadFile("/pipeline/" + pipelineUuid + "/result", tempZipFile);
                FileUtils.unzipFile(tempZipFile, output);
                tempZipFile.delete();
            } catch (InterruptedException | IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static HashMap<String, String> getAuthorizationHeaders(String remoteUserToken, String remoteUserTenant, String remoteApiToken) {
        HashMap<String, String> headers = new HashMap<>();
        if (remoteUserToken != null) {
            headers.put("X-User-Token", remoteUserToken);
            headers.put("X-User-Tenant", remoteUserTenant);
        } else if (remoteApiToken != null) {
            headers.put("X-Api-Token", remoteApiToken);
        }
        return headers;
    }

    private static void runWorkflowLocally(HashMap<String, String> environmentVariables, Workflow workflow, byte[] inputData, File output) {
        Pipeline pipeline = new Pipeline(UUID.randomUUID(),
                "main",
                workflow,
                environmentVariables,
                null,
                null,
                getPipelineData(workflow));
        pipeline.computeSteps();
        PipelineRunner pipelineRunner = new PipelineRunner(pipeline, pipelineFolder -> {
            try {
                FileUtils.deleteDir(output);
                Files.walk(pipelineFolder.toPath()).forEach(source -> {
                    Path destination = Paths.get(output.getAbsolutePath(), source.toString().substring(pipelineFolder.getAbsolutePath().length()));
                    try {
                        Files.copy(source, destination);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
            return true;
        }, pipelineStatus -> System.out.println(new PipelineLogLine(new UUID(0, 0), "Status changed to: " + pipelineStatus.name())),
                System.out::println);
        pipelineRunner.runPipeline(inputData, false);
    }

    private static byte[] readInputData(String inputFilePath) {
        if (inputFilePath == null) {
            return new byte[]{};
        }
        try {
            File inputDataFile = new File(inputFilePath);
            if (!inputDataFile.exists()) {
                System.err.println("Unable to find data path " + inputDataFile.getAbsolutePath());
                System.exit(1);
            }
            return Files.readAllBytes(inputDataFile.toPath());
        } catch (IOException ignore) {
            printErrorAndExit("Unable to read input data");
        }
        return null;
    }

    private static String getWorkflowFileContent(String workflowPath) {
        try {
            if (workflowPath == null) {
                if (System.in.available() == 0) {
                    printUsageAndExit();
                    return null;
                }
                StringBuilder inputData = new StringBuilder();
                try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
                    String line;
                    while ((line = br.readLine()) != null) {
                        inputData.append(line).append(System.lineSeparator());
                    }
                }
                return inputData.toString();
            }
            File workflowFile = new File(workflowPath);
            if (!workflowFile.exists()) {
                printErrorAndExit("Unable to find workflow " + workflowFile.getAbsolutePath());
            }
            return Files.readString(workflowFile.toPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static PipelineData getPipelineData(Workflow workflow) {
        PipelineData pipelineData = new PipelineData();
        ArrayList<Integer> cpuCores = getRandomCpuCores(workflow);
        pipelineData.setCpuCores(cpuCores);
        return pipelineData;
    }

    private static ArrayList<Integer> getRandomCpuCores(Workflow workflow) {
        int cpuCoresCount = Runtime.getRuntime().availableProcessors(); // Get threads count
        ArrayList<Integer> cpuCores = new ArrayList<>(cpuCoresCount);
        for (int i = 0; i < cpuCoresCount; i++) {
            cpuCores.add(i);
        }
        Collections.shuffle(cpuCores);
        while (cpuCores.size() > workflow.getCpuCount()) {
            cpuCores.remove(0);
        }
        return cpuCores;
    }

    private static void printErrorAndExit(String message) {
        System.err.println(message);
        System.exit(1);
    }

    private static void printUsageAndExit() {
        //TODO Use an external file
        System.err.println("Usage [--in input.data] [-e KEY=value] run workflow.yaml");
        System.err.println("");
        System.err.println("tenant list - List tenants");
        System.err.println("tenant add - Add the tenant specified with --remote-tenant");
        System.err.println("tenant remove - Remove the tenant specified with --remote-tenant");
        System.err.println("");
        System.err.println("apitoken list - List selected tenant apitokens");
        System.err.println("apitoken add [comment] - Add a new apitoken to this tenant");
        System.err.println("apitoken remove <token> - Remove the selected apitoken");
        System.err.println("");
        System.err.println("--remote url");
        System.err.println("Perform the task on a remote scheduler, (eg: --remote https://maas.cri.epita.fr)");
        System.err.println("You must be authenticated with a api token (eg: --remote-api-token 263d8236-58cf-4180-baaa-bffbcf0b3c43)" +
                "or a user token and tenant (eg: --remote-user-token 263d8236-58cf-4180-baaa-bffbcf0b3c43 --remote-tenant main).");
        System.err.println("Api tokens are linked to a tenant and user tokens can access all the tenants they have right on");
        System.exit(1);
    }
}
