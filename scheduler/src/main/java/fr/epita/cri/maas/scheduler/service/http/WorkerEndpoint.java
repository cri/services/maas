package fr.epita.cri.maas.scheduler.service.http;

import fr.epita.cri.maas.scheduler.schedulers.RemoteScheduler;
import fr.epita.cri.maas.scheduler.service.SchedulerService;
import fr.epita.cri.maas.scheduler.service.model.WorkerModel;
import fr.epita.cri.maas.scheduler.service.utils.AuthorizationSystem;
import fr.epita.cri.maas.scheduler.workers.RemoteWorker;
import io.vertx.core.http.HttpServerRequest;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Path("/worker")
public class WorkerEndpoint {
    @Inject AuthorizationSystem authorizationSystem;
    @Inject EntityManager entityManager;
    @Inject SchedulerService schedulerService;
    @Context HttpServerRequest request;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    public Response getWorkers() {
        if (!authorizationSystem.isClientAdmin(request)) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        List<WorkerModel> workerModels = entityManager.createQuery("select wm from WorkerModel wm", WorkerModel.class).getResultList();
        List<String> workerNames = workerModels.stream().map(WorkerModel::getName).collect(Collectors.toList());
        return Response.ok(workerNames).build();
    }

    @DELETE
    @Path("{worker_name}")
    @Transactional
    public Response deleteWorker(@PathParam("worker_name") String workerName) {
        if (!authorizationSystem.isClientAdmin(request)) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        List<WorkerModel> workerModels = entityManager.createQuery("select wm from WorkerModel wm where wm.name = :name", WorkerModel.class)
                .setParameter("name", workerName)
                .getResultList();

        if (workerModels.isEmpty()) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }

        WorkerModel workerModel = workerModels.get(0);
        schedulerService.getSchedulerWebsocketServer().removeRemoteWorker(workerModel.getName());
        entityManager.remove(workerModel);

        return Response.ok().build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Response addWorker(String workerName) {
        if (!authorizationSystem.isClientAdmin(request)) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        if (workerName == null || !workerName.matches("[A-Za-z0-9._-]+")) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        List<WorkerModel> workerModels = entityManager.createQuery("select wm from WorkerModel wm where wm.name = :name", WorkerModel.class)
                .setParameter("name", workerName)
                .getResultList();

        if (!workerModels.isEmpty()) {
            return Response.status(Response.Status.CONFLICT).build();
        }

        WorkerModel workerModel = new WorkerModel(workerName, UUID.randomUUID().toString());
        entityManager.persist(workerModel);
        addWorker(workerModel.getName(), workerModel.getAuthenticationToken());

        return Response.ok(workerModel.getAuthenticationToken()).build();
    }

    /**
     * Update or create a worker, if he can provide the worker registration token
     *
     * @param workerName The worker name, taken from the worker hostname
     * @return The worker token
     */
    @PUT
    @Path("{worker_name}")
    @Transactional
    public Response autoUpdateWorker(@PathParam("worker_name") String workerName) {
        if (!authorizationSystem.hasValidWorkerRegisterToken(request)) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        List<WorkerModel> workerModels = entityManager.createQuery("select wm from WorkerModel wm where wm.name = :name", WorkerModel.class)
                .setParameter("name", workerName)
                .getResultList();

        WorkerModel workerModel;
        if (!workerModels.isEmpty()) {
            workerModel = workerModels.get(0);
        } else {
            workerModel = new WorkerModel(workerName, UUID.randomUUID().toString());
            entityManager.persist(workerModel);
            addWorker(workerModel.getName(), workerModel.getAuthenticationToken());
        }

        return Response.ok(workerModel.getAuthenticationToken()).build();
    }

    private void addWorker(String name, String authenticationToken) {
        RemoteWorker remoteWorker = new RemoteWorker(name, authenticationToken);
        remoteWorker.setScheduler(schedulerService.getScheduler());
        schedulerService.getSchedulerWebsocketServer().addRemoteWorker(remoteWorker);
        if (schedulerService.getScheduler() instanceof RemoteScheduler) {
            ((RemoteScheduler) schedulerService.getScheduler()).addWorker(remoteWorker);
        }
    }
}
