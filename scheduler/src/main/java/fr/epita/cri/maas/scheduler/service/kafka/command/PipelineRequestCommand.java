package fr.epita.cri.maas.scheduler.service.kafka.command;

import com.fasterxml.jackson.annotation.JsonCreator;
import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.*;

import java.util.HashMap;
import java.util.UUID;

@RegisterForReflection
@With
@Getter
@EqualsAndHashCode
@ToString
@AllArgsConstructor(onConstructor = @__({@JsonCreator}))
public class PipelineRequestCommand {
    public UUID id;
    public String tenant;
    public String workflowYaml;
    public HashMap<String, String> variables;
    public String sourceDataUrl;
    private String statusCallbackUrl;
}
