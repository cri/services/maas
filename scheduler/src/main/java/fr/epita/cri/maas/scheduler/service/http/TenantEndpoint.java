package fr.epita.cri.maas.scheduler.service.http;

import fr.epita.cri.maas.scheduler.service.model.ApiTokenModel;
import fr.epita.cri.maas.scheduler.service.model.TenantModel;
import fr.epita.cri.maas.scheduler.service.model.UserModel;
import fr.epita.cri.maas.scheduler.service.utils.AuthorizationSystem;
import io.vertx.core.http.HttpServerRequest;
import lombok.AllArgsConstructor;
import lombok.Value;
import lombok.With;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Path("/tenant")
public class TenantEndpoint {
    @Inject AuthorizationSystem authorizationSystem;
    @Inject EntityManager entityManager;
    @Context HttpServerRequest request;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    public Response getTenants() {
        if (!authorizationSystem.isClientAdmin(request)) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        List<TenantModel> tenants = entityManager.createQuery("select t from TenantModel t", TenantModel.class).getResultList();
        List<String> tenantsNames = tenants.stream().map(TenantModel::getName).collect(Collectors.toList());
        return Response.ok(tenantsNames).build();
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    public Response addTenant(CreateTenantRequest createTenantRequest) {
        String imageRegex = createTenantRequest.imageRegex;
        if (!authorizationSystem.isClientAdmin(request)) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        String tenantName = authorizationSystem.getUserTenantName(request);

        //Ensuring a tenant with this name does not exists
        List<TenantModel> tenants = entityManager.createQuery("select t from TenantModel t where t.name = :name", TenantModel.class)
                .setParameter("name", tenantName)
                .getResultList();
        if (!tenants.isEmpty()) {
            return Response.status(Response.Status.CONFLICT).build();
        }

        TenantModel tenantModel = new TenantModel(tenantName, imageRegex, createTenantRequest.hostPathsRegex);
        entityManager.persist(tenantModel);

        UserModel userModel = authorizationSystem.getClientUserModel(request);
        userModel.getAllowedTenants().add(tenantModel);
        entityManager.persist(userModel);

        return Response.ok().build();
    }

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    public Response deleteTenant() {
        if (!authorizationSystem.isClientAdmin(request)) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        String tenantName = authorizationSystem.getUserTenantName(request);

        //Ensuring a tenant with this name exists
        List<TenantModel> tenants = entityManager.createQuery("select t from TenantModel t where t.name = :name", TenantModel.class)
                .setParameter("name", tenantName)
                .getResultList();
        if (tenants.isEmpty()) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }

        TenantModel tenantModel = tenants.get(0);

        List<UserModel> userModels = entityManager.createQuery("select ut from UserModel ut", UserModel.class).getResultList();
        for (UserModel userModel : userModels) {
            boolean tenantRemoved = userModel.getAllowedTenants().remove(tenantModel);
            if (tenantRemoved) {
                entityManager.persist(userModel);
            }
        }

        entityManager.remove(tenantModel);

        return Response.ok().build();
    }

    @POST
    @Path("apitoken/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    public Response createApiToken(String comment) {
        TenantModel tenantModel = authorizationSystem.getClientTenant(request);
        if (tenantModel == null || !authorizationSystem.isUser(request)) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }

        ApiTokenModel apiTokenModel = new ApiTokenModel(comment, tenantModel);
        entityManager.persist(apiTokenModel);
        return Response.ok(apiTokenModel.getToken()).build();
    }

    @PUT
    @Path("apitoken/{token}")
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    public Response updateApiToken(@PathParam("token") UUID token, String description) {
        TenantModel tenantModel = authorizationSystem.getClientTenant(request);
        if (tenantModel == null || !authorizationSystem.isUser(request)) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }

        ApiTokenModel apiTokenModel = entityManager.createQuery("select at from ApiTokenModel at where at.token = :token", ApiTokenModel.class)
                .setParameter("token", token)
                .getResultList()
                .stream()
                .findAny()
                .orElse(null);

        if (apiTokenModel == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }

        apiTokenModel.setDescription(description);
        entityManager.persist(apiTokenModel);

        return Response.ok().build();
    }

    @DELETE
    @Path("apitoken/{token}")
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    public Response deleteApiToken(@PathParam("token") UUID token) {
        TenantModel tenantModel = authorizationSystem.getClientTenant(request);
        if (tenantModel == null || (!authorizationSystem.isUser(request) && !authorizationSystem.isSelfApiToken(request, token))) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }

        ApiTokenModel apiTokenModel = entityManager.createQuery("select at from ApiTokenModel at where at.token = :token", ApiTokenModel.class)
                .setParameter("token", token)
                .getResultList()
                .stream()
                .findAny()
                .orElse(null);

        if (apiTokenModel == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }

        entityManager.remove(apiTokenModel);

        return Response.ok().build();
    }

    @AllArgsConstructor
    @Value
    public static class CreateTenantRequest {
        public String imageRegex;
        public String hostPathsRegex;
    }
}
