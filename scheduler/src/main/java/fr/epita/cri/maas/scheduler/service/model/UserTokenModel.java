package fr.epita.cri.maas.scheduler.service.model;

import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.time.Instant;
import java.util.UUID;

@Getter
@Entity
@NoArgsConstructor
public class UserTokenModel {
    @Id private UUID token;
    @ManyToOne private UserModel user;
    private Instant expiration;

    public UserTokenModel(UserModel user, Instant expiration) {
        token = UUID.randomUUID();//I am not sure hibernate use a secured random source to generate UUIDs
        this.user = user;
        this.expiration = expiration;
    }
}
