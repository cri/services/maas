package fr.epita.cri.maas.scheduler.service.kafka.event;

import com.fasterxml.jackson.annotation.JsonCreator;
import fr.epita.cri.maas.data.pipeline.PipelineStatus;
import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.*;

import java.util.UUID;

@RegisterForReflection
@With
@Getter
@EqualsAndHashCode
@ToString
@AllArgsConstructor(onConstructor = @__({@JsonCreator}))
public class PipelineStatusEvent {
    public UUID id;
    public PipelineStatus status;
}
