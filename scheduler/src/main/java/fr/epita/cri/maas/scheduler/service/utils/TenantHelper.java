package fr.epita.cri.maas.scheduler.service.utils;

import fr.epita.cri.maas.scheduler.service.model.TenantModel;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@ApplicationScoped
public class TenantHelper {
    @Inject EntityManager em;

    public List<TenantModel> getAllowedTenantsByRoles(List<String> roles) {
        return roles.stream()
                .map(this::getTenantByName)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    public TenantModel getTenantByName(String name) {
        return em.createQuery("select t from TenantModel t where t.name = :name", TenantModel.class)
                .setParameter("name", name)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }
}
