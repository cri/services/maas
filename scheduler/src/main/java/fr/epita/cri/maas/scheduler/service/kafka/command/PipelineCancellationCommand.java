package fr.epita.cri.maas.scheduler.service.kafka.command;

import com.fasterxml.jackson.annotation.JsonCreator;
import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.*;

import java.time.Instant;
import java.util.UUID;

@RegisterForReflection
@With
@Getter
@EqualsAndHashCode
@ToString
@AllArgsConstructor(onConstructor = @__({@JsonCreator}))
public class PipelineCancellationCommand {
    public UUID id;
    private Instant cancellationTime;
}
