package fr.epita.cri.maas.scheduler.service.kafka;

import fr.epita.cri.maas.scheduler.service.SchedulerService;
import fr.epita.cri.maas.scheduler.service.kafka.command.PipelineCancellationCommand;
import fr.epita.cri.maas.scheduler.service.kafka.command.PipelineRequestCommand;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class PipelineConsumer {
    @Inject SchedulerService schedulerService;

    public void onPipelineRequestCommand(PipelineRequestCommand pipelineRequestCommand) {
    }

    public void onPipelineCancellationCommand(PipelineCancellationCommand pipelineCancellationCommand) {
    }
}
