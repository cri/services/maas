package fr.epita.cri.maas.scheduler.service.kafka;

import fr.epita.cri.maas.scheduler.service.kafka.event.PipelineResultEvent;
import fr.epita.cri.maas.scheduler.service.kafka.event.PipelineStatusEvent;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class PipelineProducer {
    public void sendPipelineResultEvent(PipelineResultEvent pipelineResultEvent) {
    }

    public void sendPipelineStatusEvent(PipelineStatusEvent pipelineStatusEvent) {
    }
}
