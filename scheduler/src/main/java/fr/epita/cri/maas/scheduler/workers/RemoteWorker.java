package fr.epita.cri.maas.scheduler.workers;

import fr.epita.cri.maas.message.worker.WorkerDownstreamMessage;
import fr.epita.cri.maas.message.worker.WorkerMessage;
import fr.epita.cri.maas.message.worker.WorkerUpstreamMessage;
import fr.epita.cri.maas.scheduler.service.SchedulerService;
import fr.epita.cri.maas.websocket.WebsocketSerializer;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.java_websocket.WebSocket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RequiredArgsConstructor
public class RemoteWorker extends Worker {
    private static final Logger logger = LoggerFactory.getLogger(RemoteWorker.class);
    @Getter
    private final String name;
    @Getter
    private final String authenticationToken;
    private WebSocket webSocket;
    private long disconnectedSince = 0;

    @Override
    public void sendDownstreamMessage(WorkerDownstreamMessage workerMessage) {
        if (webSocket != null && webSocket.isOpen()) {
            String message = WebsocketSerializer.serializeWebsocketMessage(workerMessage);
            if (message != null) {
                webSocket.send(message);
                logger.debug("Send {}", message);
            }
        }
    }

    public void onConnect(WebSocket webSocket) {
        this.webSocket = webSocket;
        this.disconnectedSince = 0;
        start();
    }

    public void onWebsocketMessage(String s) {
        WorkerMessage workerMessage = WebsocketSerializer.deserializeWebsocketMessage(s);
        if (workerMessage != null) {
            onUpstreamMessage((WorkerUpstreamMessage) workerMessage);
        }
    }

    @Override
    public boolean isRunning() {
        return super.isRunning() &&
                (disconnectedSince == 0
                        || (System.currentTimeMillis() - disconnectedSince) < 60000L // 60s timeout
                );
    }

    @Override
    public boolean isAcceptingJobs() {
        return super.isAcceptingJobs() && webSocket.isOpen();
    }

    public void onDisconnect() {
        this.disconnectedSince = System.currentTimeMillis();
    }
}
