package fr.epita.cri.maas.scheduler.service.model;

import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Getter
@Entity
@NoArgsConstructor
public class TenantModel {
    @Id private String name;
    @OneToMany(mappedBy = "tenant", cascade = CascadeType.REMOVE) private List<ApiTokenModel> tokens;
    private String imageRegex; // Used to filter docker images that can be used by this tenant

    // Path that the tenant pipeline can mount inside workflow tasks
    private String hostPathsRegex;

    public TenantModel(String name, String imageRegex, String hostPathsRegex) {
        this.name = name;
        this.imageRegex = imageRegex;
        this.hostPathsRegex = hostPathsRegex;
    }
}
