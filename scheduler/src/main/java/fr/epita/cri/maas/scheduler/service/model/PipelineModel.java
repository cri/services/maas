package fr.epita.cri.maas.scheduler.service.model;

import fr.epita.cri.maas.data.pipeline.PipelineStatus;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.Instant;
import java.util.UUID;

@Entity
@Table(indexes = @Index(columnList = "relevant"))
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class PipelineModel {
    @Id private UUID pipelineId;
    @Setter @Enumerated(EnumType.STRING) private PipelineStatus status;
    @Setter private Instant lastStatusUpdate;
    @Setter private byte[] pipelineData;
    @ManyToOne(optional = false) private TenantModel tenant;
    @Setter private boolean relevant; // If the pipeline is still relevant and need to be loaded on statup, will be set to false after 24h
}
