package fr.epita.cri.maas.scheduler.schedulers;

import fr.epita.cri.maas.data.pipeline.Pipeline;
import fr.epita.cri.maas.scheduler.workers.RemoteWorker;
import fr.epita.cri.maas.scheduler.workers.Worker;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class RemoteScheduler extends Scheduler {
    private final List<RemoteWorker> potentialsWorkers;

    public RemoteScheduler(Consumer<Pipeline> pipelineResultConsumer, Consumer<Pipeline> pipelineUpdateConsumer, List<RemoteWorker> potentialsWorkers) {
        super(pipelineResultConsumer, pipelineUpdateConsumer);
        this.potentialsWorkers = new ArrayList<>(potentialsWorkers);
        for (Worker worker : potentialsWorkers) {
            worker.setScheduler(this);
        }
    }

    @Override
    public Iterable<Worker> getWorkers() {
        return potentialsWorkers.stream()
                .filter(RemoteWorker::isRunning)
                .collect(Collectors.toList());
    }

    public void addWorker(RemoteWorker remoteWorker) {
        this.potentialsWorkers.add(remoteWorker);
    }
}
