package fr.epita.cri.maas.scheduler.service.http;

import fr.epita.cri.maas.data.http.GetUserTokenResponse;
import fr.epita.cri.maas.scheduler.service.model.TenantModel;
import fr.epita.cri.maas.scheduler.service.model.UserModel;
import fr.epita.cri.maas.scheduler.service.model.UserTokenModel;
import fr.epita.cri.maas.scheduler.service.utils.TenantHelper;
import io.quarkus.oidc.IdToken;
import io.quarkus.security.Authenticated;
import org.eclipse.microprofile.jwt.JsonWebToken;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Path("/complete/epita")
public class OidcEndpoint {
    @Inject @IdToken JsonWebToken idToken;
    @Inject EntityManager em;
    @Inject TenantHelper tenantHelper;

    @GET
    @Authenticated
    @Transactional
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTokens() {
        String login = idToken.getClaim("nickname");
        String displayName = idToken.getClaim("name");
        UserModel userModel = em.find(UserModel.class, login);

        //FIXME This is possibly the worst way of doing this
        String[] rolesString = this.idToken.getClaim("roles").toString().split("(\\[\"|\",\"|\"])");
        boolean admin = Arrays.asList(rolesString).contains("admin");

        List<TenantModel> allowedTenants;
        if (admin) {
            allowedTenants = em.createQuery("select t from TenantModel t", TenantModel.class).getResultList();
        } else {
            allowedTenants = tenantHelper.getAllowedTenantsByRoles(List.of(rolesString));
        }

        if (userModel == null) {
            userModel = new UserModel(login, displayName, admin, new ArrayList<>());
        }

        //Refreshing allowed tenants
        userModel.getAllowedTenants().clear();
        userModel.getAllowedTenants().addAll(allowedTenants);

        em.persist(userModel);

        //FIXME Don't do that here
        removeExpiredUserTokens(login);

        UserTokenModel userTokenModel = new UserTokenModel(userModel, Instant.now().plus(1, ChronoUnit.DAYS));
        em.persist(userTokenModel);

        List<String> tenantsNames = allowedTenants.stream().map(TenantModel::getName).collect(Collectors.toList());
        GetUserTokenResponse getUserTokenResponse = new GetUserTokenResponse(userTokenModel.getToken(), tenantsNames);
        return Response.ok(getUserTokenResponse).build();
    }

    private void removeExpiredUserTokens(String login) {
        //Get all tokens matching this user
        //FIXME find how to filter on instant
        List<UserTokenModel> userTokenModels = em.createQuery("select ut from UserTokenModel ut where ut.user.login = :login", UserTokenModel.class)
                .setParameter("login", login)
                .getResultList();

        //Remove expired tokens
        userTokenModels.stream()
                .filter(t -> t.getExpiration().isBefore(Instant.now()))
                .forEach(em::remove);
    }
}
