package fr.epita.cri.maas.scheduler.service.http;

import org.eclipse.microprofile.openapi.annotations.Components;
import org.eclipse.microprofile.openapi.annotations.OpenAPIDefinition;
import org.eclipse.microprofile.openapi.annotations.enums.SecuritySchemeIn;
import org.eclipse.microprofile.openapi.annotations.enums.SecuritySchemeType;
import org.eclipse.microprofile.openapi.annotations.info.Info;
import org.eclipse.microprofile.openapi.annotations.security.SecurityRequirement;
import org.eclipse.microprofile.openapi.annotations.security.SecurityScheme;

import javax.ws.rs.core.Application;

@OpenAPIDefinition(
        info = @Info(title = "MaaS Scheduler API", version = "0.0.1"),
        components = @Components(
                securitySchemes = {
                        @SecurityScheme(
                                securitySchemeName = "ApiKeyAuth",
                                type = SecuritySchemeType.APIKEY,
                                in = SecuritySchemeIn.HEADER,
                                apiKeyName = "X-Api-Token"
                        )
                }
        ),
        security = @SecurityRequirement(name = "ApiKeyAuth")
)
public class SchedulerApiApplication extends Application {
}
