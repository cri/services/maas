package fr.epita.cri.maas.scheduler.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import fr.epita.cri.maas.data.http.PipelineStatusBody;
import fr.epita.cri.maas.data.pipeline.*;
import fr.epita.cri.maas.scheduler.SchedulerWebsocketServer;
import fr.epita.cri.maas.scheduler.schedulers.RemoteScheduler;
import fr.epita.cri.maas.scheduler.schedulers.Scheduler;
import fr.epita.cri.maas.scheduler.service.kafka.PipelineProducer;
import fr.epita.cri.maas.scheduler.service.model.PipelineModel;
import fr.epita.cri.maas.scheduler.service.model.TenantModel;
import fr.epita.cri.maas.scheduler.service.model.WorkerModel;
import fr.epita.cri.maas.scheduler.service.utils.S3Utils;
import fr.epita.cri.maas.scheduler.service.utils.SchedulerConfig;
import fr.epita.cri.maas.scheduler.service.utils.TenantHelper;
import fr.epita.cri.maas.scheduler.workers.RemoteWorker;
import fr.epita.cri.maas.util.SerializerUtils;
import io.micrometer.core.instrument.MeterRegistry;
import io.quarkus.runtime.Startup;
import io.quarkus.scheduler.Scheduled;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@ApplicationScoped
@Getter
@Startup
public class SchedulerService {
    private static final Logger logger = LoggerFactory.getLogger(SchedulerService.class);
    private final ConcurrentHashMap<UUID, Pipeline> pipelines = new ConcurrentHashMap<>();
    @Inject S3Utils s3Utils;
    @Inject SchedulerConfig schedulerConfig;
    @Inject PipelineProducer pipelineProducer;
    @Inject TenantHelper tenantHelper;
    @Inject EntityManager entityManager;
    @Inject MeterRegistry meterRegistry;
    private Scheduler scheduler;
    private BlockingQueue<Pipeline> waitingPipelinesUpdates = new LinkedBlockingQueue<>();
    @Getter private SchedulerWebsocketServer schedulerWebsocketServer;

    @PostConstruct
    void postConstruct() {
        logger.info("Starting...");
        List<RemoteWorker> remoteWorkers = getRemoteWorkers();

        scheduler = new RemoteScheduler(this::onPipelineEnd, this::onPipelineDataUpdate, remoteWorkers);

        List<Pipeline> notFinishedPipeline = loadOldPipelines();

        schedulerWebsocketServer = new SchedulerWebsocketServer(remoteWorkers, 8083);
        schedulerWebsocketServer.start();

        rescheduleOldPipelines(remoteWorkers, notFinishedPipeline);

        meterRegistry.gauge("pipelines.to_run", scheduler, s -> s.getPipelinesToRun().size());
        meterRegistry.gauge("pipelines.working", scheduler, s -> countPipelineStatus(PipelineStatus.RUNNING, PipelineStatus.SETUP, PipelineStatus.ENDED, PipelineStatus.FAILED));
        meterRegistry.gauge("pipelines.running", scheduler, s -> countPipelineStatus(PipelineStatus.RUNNING));
        meterRegistry.gauge("pipelines.waiting", scheduler, s -> countPipelineStatus(PipelineStatus.WAITING));
    }

    private long countPipelineStatus(PipelineStatus... pipelineStatus) {
        List<PipelineStatus> pipelineStatuses = Arrays.asList(pipelineStatus);
        synchronized (scheduler.getPipelinesToRun()) {
            return scheduler.getPipelinesToRun().stream()
                    .map(Pipeline::getData)
                    .map(PipelineData::getStatus)
                    .filter(pipelineStatuses::contains)
                    .count();
        }
    }

    private List<RemoteWorker> getRemoteWorkers() {
        return entityManager.createQuery("select w from WorkerModel w", WorkerModel.class).getResultList().stream()
                .map(wm -> new RemoteWorker(wm.getName(), wm.getAuthenticationToken()))
                .collect(Collectors.toList());
    }

    private void onPipelineEnd(Pipeline pipeline) {
        logger.info("[{}] Pipeline ended, status: {}", pipeline.getId(), getPipelineStatus());
    }

    private void onPipelineDataUpdate(Pipeline pipeline) {
        waitingPipelinesUpdates.add(pipeline);
    }

    @Transactional
    public void onPipelineRequest(Pipeline pipeline, TenantModel tenant) {
        cleanupPipelines();
        logger.info("[{}] Pipeline request command, status: {}", pipeline.getId(), getPipelineStatus());

        // Persist the pipeline for later use
        persistPipeline(pipeline, tenant);

        pipelines.put(pipeline.getId(), pipeline);
        s3Utils.computePipelineResultUploadUrl(pipeline);
        scheduler.runPipeline(pipeline);
    }

    public void onPipelineCancellation(Pipeline pipeline) {
        logger.info("[{}] Pipeline cancellation command", pipeline.getId());
        //FIXME Scheduler cancel pipeline
    }

    @Scheduled(every = "1s", concurrentExecution = Scheduled.ConcurrentExecution.SKIP)
    @Transactional
    public void updatePipelinesPersistentData() {
        Pipeline pipeline;
        while ((pipeline = waitingPipelinesUpdates.poll()) != null) {
            persistPipeline(pipeline, null);
        }
    }

    private void persistPipeline(Pipeline pipeline, TenantModel tenant) {
        PipelineModel pipelineModel;

        if (tenant != null) { //We are creating a new pipeline
            pipelineModel = new PipelineModel(
                    pipeline.getId(),
                    null,
                    null,
                    null,
                    tenant,
                    true);
        } else { //We are updating an existing pipeline
            List<PipelineModel> pipelineList = entityManager.createQuery("select pm from PipelineModel pm where pm.pipelineId = :pipelineId", PipelineModel.class)
                    .setParameter("pipelineId", pipeline.getId())
                    .getResultList();
            if (pipelineList.isEmpty()) {
                logger.info("[{}] Unable to find pipeline in database to persist status: {}", pipeline.getId(), pipeline.getData().getStatus());
                return;
            }
            pipelineModel = pipelineList.get(0);
        }

        try {
            String pipelineData = SerializerUtils.getJsonObjectMapper().writeValueAsString(pipeline);
            pipelineModel.setStatus(pipeline.getData().getStatus());
            pipelineModel.setLastStatusUpdate(pipeline.getData().getLastStatusUpdate());
            pipelineModel.setPipelineData(pipelineData.getBytes(StandardCharsets.UTF_8));
            entityManager.persist(pipelineModel);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    public List<Pipeline> loadOldPipelines() {
        ArrayList<Pipeline> notFinishedPipelines = new ArrayList<>();
        AtomicInteger dbOffset = new AtomicInteger();

        // Compute not finished pipelines until we don't find any
        while (computeFirstNotFinishedPipelines(notFinishedPipelines, dbOffset)) {
            logger.info("Loaded {} not finished pipeline...", notFinishedPipelines.size());
        }

        return notFinishedPipelines;
    }

    @Transactional
    public boolean computeFirstNotFinishedPipelines(ArrayList<Pipeline> notFinishedPipelines, AtomicInteger dbOffset) {
        //We querry pipelines with a limit because otherwise it might cause a transaction timeout
        List<PipelineModel> pipelineModels = entityManager.createQuery("select pm from PipelineModel pm where pm.relevant = true", PipelineModel.class)
                .setMaxResults(100)
                .setFirstResult(dbOffset.get())
                .getResultList();


        for (PipelineModel pipelineModel : pipelineModels) {
            Instant expirationThreshold = getPipelineExpirationThreshold();
            if (pipelineModel.getStatus().isDone() && pipelineModel.getLastStatusUpdate().isBefore(expirationThreshold)) { // Pipeline is not relevant anymore
                pipelineModel.setRelevant(false);
                pipelineModel.setPipelineData(null);
                entityManager.persist(pipelineModel);
                continue;
            }

            dbOffset.incrementAndGet();

            try {
                String pipelineData = new String(pipelineModel.getPipelineData(), StandardCharsets.UTF_8);
                Pipeline pipeline = SerializerUtils.getJsonObjectMapper().readValue(pipelineData, Pipeline.class);
                logger.info("[{}] Old pipeline loaded with status {}", pipeline.getId(), pipeline.getData().getStatus());
                pipelines.put(pipeline.getId(), pipeline);
                if (!pipeline.getData().getStatus().isDone()) {
                    notFinishedPipelines.add(pipeline);
                }
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }

        //Return if we found pipelines in db so the method can be called again for the remaining ones
        return !pipelineModels.isEmpty();
    }

    private void rescheduleOldPipelines(List<RemoteWorker> remoteWorkers, List<Pipeline> notFinishedPipeline) {
        ScheduledExecutorService schedulerExecutor = Executors.newScheduledThreadPool(1);
        Duration recoveredPipelineRescheduleAfter = schedulerConfig.getRecoveredPipelineRescheduleAfter();
        schedulerExecutor.schedule(() -> {
            try {
                List<Pipeline> runningPipelines = remoteWorkers.stream()
                        .flatMap(rm -> new ArrayList<>(rm.getPipelines()).stream())
                        .collect(Collectors.toList());

                notFinishedPipeline.removeAll(runningPipelines);
                synchronized (scheduler.getPipelinesToRun()) {
                    notFinishedPipeline.removeAll(scheduler.getPipelinesToRun());
                }

                for (Pipeline pipeline : notFinishedPipeline) {
                    s3Utils.computePipelineResultUploadUrl(pipeline);
                    scheduler.runPipeline(pipeline);
                }

                logger.info("Rescheduled {} old pipelines", notFinishedPipeline.size());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }, recoveredPipelineRescheduleAfter.getSeconds(), TimeUnit.SECONDS);
        logger.info("Waiting {} seconds before rescheduling old pipelines", recoveredPipelineRescheduleAfter.getSeconds());
    }

    @Transactional
    public void cleanupPipelines() {
        Instant expirationThreshold = getPipelineExpirationThreshold();
        List<UUID> cleanedUpUuids = pipelines.entrySet().stream()
                .filter(e -> {
                    PipelineData pipelineData = e.getValue().getData();
                    return pipelineData.getStatus().isDone() && pipelineData.getLastStatusUpdate().isBefore(expirationThreshold);
                })
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());

        cleanedUpUuids.forEach(pipelines::remove);

        for (UUID pipelineId : cleanedUpUuids) {

            List<PipelineModel> pipelineList = entityManager.createQuery("select pm from PipelineModel pm where pm.pipelineId = :pipelineId", PipelineModel.class)
                    .setParameter("pipelineId", pipelineId)
                    .getResultList();
            if (pipelineList.isEmpty()) {
                logger.info("[{}] Unable to find pipeline in database when cleaning up", pipelineId);
                continue;
            }
            PipelineModel pipelineModel = pipelineList.get(0);
            pipelineModel.setRelevant(false);
            pipelineModel.setPipelineData(null);
            entityManager.persist(pipelineModel);
            logger.debug("[{}] Making pipeline irrelevant", pipelineId);
        }
    }

    public Instant getPipelineExpirationThreshold() {
        return Instant.now().minus(Duration.ofHours(1));
    }

    public Pipeline getPipeline(UUID pipelineId) {
        return pipelines.get(pipelineId);
    }

    public List<PipelineStatusBody> listRunningPipelines(final String tenantName) {
        return pipelines.values().stream()
                .filter(pipeline -> pipeline.getTenant().equals(tenantName))
                .filter(pipeline -> !pipeline.getData().getStatus().isDone())
                .map(pipeline -> new PipelineStatusBody(pipeline.getId(), pipeline.getData().getStatus()))
                .collect(Collectors.toList());
    }

    public String getPipelineResultPresignedUrl(Pipeline pipeline) {
        return getPipelineResultPresignedUrl(pipeline.getId());
    }

    public String getPipelineResultPresignedUrl(UUID pipelineId) {
        return s3Utils.getFilePresignedDownloadUrl(pipelineId + ".zip");
    }

    public String getPipelineStatus() {
        HashMap<PipelineStatus, AtomicInteger> pipelineStatus = new HashMap<>();
        for (PipelineStatus ps : PipelineStatus.values()) {
            pipelineStatus.put(ps, new AtomicInteger());
        }
        //Count the number of pipeline per status
        synchronized (scheduler.getPipelinesToRun()) {
            for (Pipeline pipeline : scheduler.getPipelinesToRun()) {
                pipelineStatus.get(pipeline.getData().getStatus()).incrementAndGet();
            }
        }

        StringBuilder sb = new StringBuilder();
        pipelineStatus.forEach((key, value) -> sb.append(key.getShortname()).append(value.get()).append(" "));
        return sb.toString();
    }
}