package fr.epita.cri.maas.scheduler.service.http;

import fr.epita.cri.maas.data.pipeline.Pipeline;
import fr.epita.cri.maas.data.pipeline.PipelineData;
import fr.epita.cri.maas.data.pipeline.PipelineLogLine;
import fr.epita.cri.maas.data.workflow.Workflow;
import fr.epita.cri.maas.data.workflow.WorkflowDeserializer;
import fr.epita.cri.maas.rest.CreatePipelineData;
import fr.epita.cri.maas.scheduler.service.SchedulerService;
import fr.epita.cri.maas.scheduler.service.model.PipelineModel;
import fr.epita.cri.maas.scheduler.service.model.TenantModel;
import fr.epita.cri.maas.scheduler.service.utils.AuthorizationSystem;
import fr.epita.cri.maas.scheduler.service.utils.S3Utils;
import io.vertx.core.http.HttpServerRequest;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Path("/pipeline")
public class PipelineEndpoint {
    private final String LINE_SEPARATOR = "\r\n";
    @Inject SchedulerService schedulerService;
    @Inject S3Utils s3Utils;
    @Inject AuthorizationSystem authorizationSystem;
    @Inject EntityManager entityManager;
    @Context HttpServerRequest request;

    @GET
    @Path("/running")
    @Produces(MediaType.APPLICATION_JSON)
    public Response listRunningPipelines() {
        TenantModel tenant = authorizationSystem.getClientTenant(request);
        if (tenant == null) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }

        return Response.ok(schedulerService.listRunningPipelines(tenant.getName())).build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response task(CreatePipelineData createPipelineData) {
        TenantModel tenant = authorizationSystem.getClientTenant(request);
        if (tenant == null) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }

        Response workflowErrorResponse = checkWorkflow(createPipelineData.getWorkflowYaml());
        if (workflowErrorResponse != null) {
            return workflowErrorResponse;
        }

        // Generating a new random uuid, returned to the client at the end
        UUID pipelineUuid = UUID.randomUUID();

        // If we need to upload the data to our s3 because the client sent it directly
        if (createPipelineData.getSourceDataUrl() == null
                && createPipelineData.getSourceData() != null
                && createPipelineData.getSourceData().length > 0) {
            String fileUrl = s3Utils.uploadFileAndGetUrl(pipelineUuid + ".in", createPipelineData.getSourceData());
            createPipelineData.setSourceDataUrl(fileUrl);
        }

        // Create and launch the pipeline
        Workflow workflow = WorkflowDeserializer.getWorkflow(createPipelineData.getWorkflowYaml());
        Pipeline pipeline = new Pipeline(pipelineUuid,
                tenant.getName(),
                workflow,
                createPipelineData.getVariables(),
                createPipelineData.getSourceDataUrl(),
                createPipelineData.getStatusCallbackUrl(),//TODO Sanitize url
                new PipelineData());

        //If we have any image that does not match the tenant regex
        for (String image : pipeline.getImages()) {
            if (!image.matches(tenant.getImageRegex())) {
                System.err.println("Tenant " + tenant.getName() + " not allowed to use image " + image + " (does not match " + tenant.getImageRegex() + ")");
                return Response.status(Response.Status.FORBIDDEN).entity("Not allowed to use image " + image).build();
            }
        }

        for (String hostPath : pipeline.getHostMounts()) {
            if (tenant.getHostPathsRegex() == null || !hostPath.matches(tenant.getHostPathsRegex())) {
                System.err.println("Tenant " + tenant.getName() + " not allowed to mount host path " + hostPath
                                   + " (does not match " + tenant.getHostPathsRegex() + ")");
                return Response.status(Response.Status.FORBIDDEN)
                               .entity("Not allowed to mount host path " + hostPath)
                               .build();
            }
        }

        schedulerService.onPipelineRequest(pipeline, tenant);

        return Response.ok(pipelineUuid).build();
    }

    @GET
    @Path("{pipeline_id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPipelineStatus(@PathParam("pipeline_id") UUID pipelineId) {
        Pipeline pipeline = schedulerService.getPipeline(pipelineId);
        if (pipeline == null) {
            return Response.status(Response.Status.NOT_FOUND).entity("Pipeline not found").build();
        }
        return Response.ok(pipeline.getData().getStatus()).build();
    }

    @GET
    @Path("{pipeline_id}/result")
    @Produces(MediaType.TEXT_PLAIN)
    @Transactional
    public Response getPipelineResult(@PathParam("pipeline_id") UUID pipelineId) {
        TenantModel tenant = authorizationSystem.getClientTenant(request);
        if (tenant == null) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }

        List<PipelineModel> pipelineList = entityManager.createQuery("select pm from PipelineModel pm where pm.pipelineId = :pipelineId", PipelineModel.class)
                .setParameter("pipelineId", pipelineId)
                .getResultList();

        if (pipelineList.isEmpty()) {
            return Response.status(Response.Status.NOT_FOUND).entity("Pipeline not found").build();
        }

        PipelineModel pipelineModel = pipelineList.get(0);
        if (!pipelineModel.getTenant().getName().equals(tenant.getName())) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        String resultDownloadUrl = schedulerService.getPipelineResultPresignedUrl(pipelineId);
        if (resultDownloadUrl != null) {
            try {
                return Response.temporaryRedirect(new URI(resultDownloadUrl)).build();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }
        return Response.serverError().build();
    }

    @GET
    @Path("{pipeline_id}/details")
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    public Response getPipelineDetails(@PathParam("pipeline_id") UUID pipelineId) {
        TenantModel tenant = authorizationSystem.getClientTenant(request);
        if (tenant == null) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }

        Pipeline pipeline = schedulerService.getPipeline(pipelineId);

        if (pipeline == null) {
            return Response.status(Response.Status.GONE).entity("Pipeline expired or not found").build();
        }

        if (!pipeline.getTenant().equals(tenant.getName())) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        return Response.ok(pipeline).build();
    }

    @GET
    @Path("{pipeline_id}/logs")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getPipelineLogs(@PathParam("pipeline_id") UUID pipelineId) {
        TenantModel tenant = authorizationSystem.getClientTenant(request);
        if (tenant == null) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }

        Pipeline pipeline = schedulerService.getPipeline(pipelineId);
        if (pipeline == null) {
            return Response.status(Response.Status.GONE).entity("Pipeline expired or not found").build();
        }
        if (!pipeline.getTenant().equals(tenant.getName())) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        return Response.ok(pipeline.getLogLines().stream()
                .map(PipelineLogLine::toString)
                .collect(Collectors.joining(LINE_SEPARATOR))
        ).build();
    }

    @GET
    @Path("{pipeline_id}/logs/{container_uuid}")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getPipelineContainerLogs(@PathParam("pipeline_id") UUID pipelineId, @PathParam("container_uuid") UUID containerUuid) {
        TenantModel tenant = authorizationSystem.getClientTenant(request);
        if (tenant == null) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }

        Pipeline pipeline = schedulerService.getPipeline(pipelineId);
        if (pipeline == null) {
            return Response.status(Response.Status.GONE).entity("Pipeline expired or not found").build();
        }
        if (!pipeline.getTenant().equals(tenant.getName())) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        return Response.ok(pipeline.getLogLines().stream()
                .filter(ll -> ll.getContainerUuid().equals(containerUuid))
                .map(PipelineLogLine::getLogLine)
                .collect(Collectors.joining(LINE_SEPARATOR))
        ).build();
    }

    @DELETE
    @Path("{pipeline_id}")
    public Response cancelPipeline(@PathParam("pipeline_id") UUID pipelineId) {
        TenantModel tenant = authorizationSystem.getClientTenant(request);
        if (tenant == null) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }

        Pipeline pipeline = schedulerService.getPipeline(pipelineId);
        if (pipeline == null) {
            return Response.status(Response.Status.BAD_REQUEST).entity("Pipeline not found").build();
        }
        if (!pipeline.getTenant().equals(tenant.getName())) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        schedulerService.onPipelineCancellation(pipeline);

        return Response.ok().build();
    }

    private Response checkWorkflow(String workflowYaml) {
        Workflow workflow = WorkflowDeserializer.getWorkflow(workflowYaml);
        if (workflow == null) {
            return Response.status(Response.Status.BAD_REQUEST).entity("Unable to parse workflow").build();
        }
        try {
            workflow.sanitize();
        } catch (IllegalArgumentException e) {
            return Response.status(Response.Status.BAD_REQUEST).entity("Invalid workflow: " + e.getMessage()).build();
        }
        return null;
    }
}
