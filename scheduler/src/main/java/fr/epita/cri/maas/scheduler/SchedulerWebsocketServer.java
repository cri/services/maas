package fr.epita.cri.maas.scheduler;

import fr.epita.cri.maas.scheduler.workers.RemoteWorker;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SchedulerWebsocketServer extends WebSocketServer {
    private static final Logger logger = LoggerFactory.getLogger(RemoteWorker.class);
    private final List<RemoteWorker> remoteWorkers;
    private Map<WebSocket, RemoteWorker> websocketRemoteWorker = new HashMap<>();

    public SchedulerWebsocketServer(List<RemoteWorker> remoteWorkers, int port) {
        super(new InetSocketAddress(port));
        setReuseAddr(true);
        this.remoteWorkers = remoteWorkers;
    }

    @Override
    public void onOpen(WebSocket webSocket, ClientHandshake clientHandshake) {

    }

    @Override
    public void onMessage(WebSocket webSocket, String s) {
        logger.debug("Received {}", s);
        RemoteWorker remoteWorker = websocketRemoteWorker.get(webSocket);
        if (remoteWorker != null) {
            remoteWorker.onWebsocketMessage(s);
        } else if (s.startsWith("AUTH")) {
            String token = s.substring(5);
            remoteWorker = remoteWorkers.stream()
                    .filter(rw -> rw.getAuthenticationToken().equals(token))
                    .findAny().orElse(null);

            if (remoteWorker != null) {
                websocketRemoteWorker.put(webSocket, remoteWorker);
                remoteWorker.onConnect(webSocket);
            } else {
                webSocket.send("Authentication failed");
                webSocket.close();
            }
        }
    }

    @Override
    public void onClose(WebSocket webSocket, int i, String s, boolean b) {
        cleanupWorkerWebsocket(webSocket);
    }

    @Override
    public void onError(WebSocket webSocket, Exception e) {
        e.printStackTrace();
        cleanupWorkerWebsocket(webSocket);
    }

    private void cleanupWorkerWebsocket(WebSocket webSocket) {
        RemoteWorker remoteWorker = websocketRemoteWorker.remove(webSocket);
        if (remoteWorker != null) {
            remoteWorker.onDisconnect();
        }
    }

    @Override
    public void onStart() {

    }

    public void removeRemoteWorker(String name) {
        RemoteWorker remoteWorker = remoteWorkers.stream()
                .filter(rm -> rm.getName().equals(name))
                .findAny().orElse(null);

        if (remoteWorker == null) {
            return;
        }

        remoteWorkers.remove(remoteWorker);
        for (Map.Entry<WebSocket, RemoteWorker> entry : websocketRemoteWorker.entrySet()) {
            if (entry.getValue().equals(remoteWorker)) {
                entry.getKey().close();
            }
        }
    }

    public void addRemoteWorker(RemoteWorker remoteWorker) {
        remoteWorkers.add(remoteWorker);
    }
}
