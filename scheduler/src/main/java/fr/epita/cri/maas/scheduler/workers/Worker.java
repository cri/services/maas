package fr.epita.cri.maas.scheduler.workers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.epita.cri.maas.data.http.PipelineStatusBody;
import fr.epita.cri.maas.data.pipeline.Pipeline;
import fr.epita.cri.maas.data.pipeline.PipelineData;
import fr.epita.cri.maas.data.pipeline.PipelineLogLine;
import fr.epita.cri.maas.data.pipeline.PipelineStatus;
import fr.epita.cri.maas.data.workflow.Workflow;
import fr.epita.cri.maas.message.worker.*;
import fr.epita.cri.maas.scheduler.schedulers.Scheduler;
import fr.epita.cri.maas.util.SerializerUtils;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.*;

@Getter
public abstract class Worker {
    private static final Logger logger = LoggerFactory.getLogger(Worker.class);
    private static final ObjectMapper objectMapper = SerializerUtils.getJsonObjectMapper();
    private static final ExecutorService pushPipelineStatusExecutorService = Executors.newSingleThreadExecutor();
    private final List<Pipeline> pipelines = Collections.synchronizedList(new ArrayList<>());
    private final ConcurrentHashMap<Pipeline, CompletableFuture<PipelineData>> pipelinesCompletableFuture = new ConcurrentHashMap<>();
    private int cpuCount;
    private int memoryMb;
    @Setter
    private Scheduler scheduler;

    public void start() {
        sendDownstreamMessage(new WorkerGetStatusMessage());
    }

    /**
     * Run a job
     *
     * @return If the job could be scheduled
     */
    public boolean runPipeline(Pipeline pipeline) {//TODO Enable job recovery
        if (!isRunning()) {
            return false;
        }
        if (isPipelineRunning(pipeline)) {
            return true;
        }

        int usedCpu = 0;
        int usedMemory = 0;
        for (Pipeline p : pipelines) {
            Workflow w = p.getWorkflow();
            usedCpu += w.getCpuCount();
            usedMemory += w.getMemoryMb();
        }
        Workflow workflow = pipeline.getWorkflow();
        if (workflow.getCpuCount() > cpuCount - usedCpu) {
            return false;
        }
        if (workflow.getMemoryMb() > memoryMb - usedMemory) {
            return false;
        }
        //TODO Check capabilities availability

        pipelines.add(pipeline);
        CompletableFuture<PipelineData> pipelineWait = createWaitFuture(pipeline);
        sendDownstreamMessage(new WorkerSchedulePipelineMessage(pipeline));
        return waitForFuture(pipeline, pipelineWait);
    }

    private boolean waitForFuture(Pipeline pipeline, CompletableFuture<PipelineData> pipelineWait) {
        boolean result = false;
        try {
            pipelineWait.get(2, TimeUnit.SECONDS);
            result = true;
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
        }
        pipelinesCompletableFuture.remove(pipeline);
        return result;
    }

    private CompletableFuture<PipelineData> createWaitFuture(Pipeline pipeline) {
        CompletableFuture<PipelineData> completableFuture = new CompletableFuture<>();
        pipelinesCompletableFuture.put(pipeline, completableFuture);
        return completableFuture;
    }

    /**
     * Interrupt a running pipeline
     *
     * @return If we were able to interupt the pipeline
     */
    public boolean interruptPipeline(Pipeline pipeline, long gracePeriod) {
        if (!isRunning()) {
            return false;
        }
        if (!isPipelineRunning(pipeline)) {
            return false;
        }

        CompletableFuture<PipelineData> pipelineWait = createWaitFuture(pipeline);

        //TODO Send message

        return waitForFuture(pipeline, pipelineWait);
    }

    /**
     * Commands to remote worker
     *
     * @param workerMessage
     */
    public abstract void sendDownstreamMessage(WorkerDownstreamMessage workerMessage);

    /**
     * Event from remote worker
     *
     * @param workerMessage
     */
    public void onUpstreamMessage(WorkerUpstreamMessage workerMessage) {
        if (workerMessage instanceof WorkerStatusMessage) {
            WorkerStatusMessage workerStatusMessage = (WorkerStatusMessage) workerMessage;
            this.cpuCount = workerStatusMessage.getCpuCount();
            this.memoryMb = workerStatusMessage.getMemoryMb();
            pipelines.removeIf(p -> !workerStatusMessage.getRunningPipelines().contains(p.getId()));
        } else if (workerMessage instanceof WorkerSetPipelineStatusMessage) {
            WorkerSetPipelineStatusMessage workerSetPipelineStatusMessage = (WorkerSetPipelineStatusMessage) workerMessage;
            Pipeline remotePipeline = workerSetPipelineStatusMessage.getPipeline();
            PipelineStatus remotePipelineStatus = remotePipeline.getData().getStatus();

            Pipeline localPipeline = getPipeline(remotePipeline.getId());
            PipelineStatus localPipelineStatus = null;

            if (localPipeline != null) {
                localPipelineStatus = localPipeline.getData().getStatus();
                localPipeline.setData(remotePipeline.getData());
            }

            if (remotePipeline.getData().getCpuCores().isEmpty()) {
                pipelines.remove(remotePipeline);
                /*if (scheduler != null) {
                    scheduler.tick();//TODO Fixme
                }*/
            } else if (localPipeline == null) {
                pipelines.add(remotePipeline);
            }
            CompletableFuture<PipelineData> completableFuture = pipelinesCompletableFuture.get(remotePipeline);
            if (completableFuture != null) {
                completableFuture.complete(remotePipeline.getData());
            }

            Pipeline finalPipeline = localPipeline == null ? remotePipeline : localPipeline;

            if (localPipelineStatus != remotePipelineStatus) {
                pushPipelineStatusToUrl(finalPipeline);
            }

            scheduler.onPipelineUpdate(finalPipeline);
        } else if (workerMessage instanceof WorkerAddPipelineContainerLogMessage) {
            WorkerAddPipelineContainerLogMessage workerAddPipelineContainerLogMessage = (WorkerAddPipelineContainerLogMessage) workerMessage;
            Pipeline localPipeline = getPipeline(workerAddPipelineContainerLogMessage.getPipelineUuid());
            if (localPipeline != null) {
                PipelineLogLine logLine = new PipelineLogLine(workerAddPipelineContainerLogMessage.getContainerUuid(), workerAddPipelineContainerLogMessage.getLogLine());
                localPipeline.getLogLines().add(logLine);
            }
        }
    }

    private boolean isPipelineRunning(Pipeline pipeline) {
        return getPipeline(pipeline.getId()) != null;
    }

    private Pipeline getPipeline(UUID pipelineId) {
        synchronized (pipelines) {
            return pipelines.stream()
                    .filter(p -> p.getId().equals(pipelineId))
                    .findFirst().orElse(null);
        }
    }

    public boolean isRunning() {
        return cpuCount > 0;
    }

    public boolean isAcceptingJobs() {
        return isRunning();
    }

    private void pushPipelineStatusToUrl(Pipeline pipeline) {
        if (pipeline.getStatusCallbackUrl() == null) {
            return;
        }
        PipelineStatusBody body = new PipelineStatusBody(pipeline.getId(), pipeline.getData().getStatus());
        try {
            String bodyString = objectMapper.writeValueAsString(body);
            byte[] bodyData = bodyString.getBytes(StandardCharsets.UTF_8);
            pushPipelineStatusExecutorService.submit(() -> {
                try {
                    URL url = new URL(pipeline.getStatusCallbackUrl());
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                    connection.setDoOutput(true);
                    connection.setRequestProperty("Content-Type", "application/json");
                    connection.setRequestMethod("POST");

                    connection.setConnectTimeout(10000);//10s timeouts
                    connection.setReadTimeout(10000);

                    OutputStream outputStream = connection.getOutputStream();
                    outputStream.write(bodyData);
                    outputStream.close();

                    if (connection.getResponseCode() >= 300) {
                        logger.error("[{}] Unable to push pipeline status (response code {}) {} {}", pipeline.getId(), connection.getResponseCode(), pipeline.getStatusCallbackUrl(), bodyString);
                    }
                } catch (IOException e) {
                    logger.error("[{}] Unable to push pipeline status {} {}", pipeline.getId(), pipeline.getStatusCallbackUrl(), bodyString);
                }
            });
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    @RequiredArgsConstructor
    @Getter
    private class PipelineWait {
        private final Pipeline pipeline;
        private final CompletableFuture<PipelineData> pipelineDataFuture;
    }
}
