package fr.epita.cri.maas.scheduler.service.utils;

import fr.epita.cri.maas.data.http.GetUserTokenResponse;
import fr.epita.cri.maas.data.pipeline.*;
import fr.epita.cri.maas.data.workflow.Workflow;
import fr.epita.cri.maas.data.workflow.WorkflowStage;
import fr.epita.cri.maas.data.workflow.WorkflowTask;
import fr.epita.cri.maas.message.worker.*;
import fr.epita.cri.maas.rest.CreatePipelineData;
import io.quarkus.runtime.annotations.RegisterForReflection;

@RegisterForReflection(targets = {
        GetUserTokenResponse.class,
        Pipeline.class,
        PipelineContainer.class,
        PipelineData.class,
        PipelineLogLine.class,
        PipelineStage.class,
        PipelineStatus.class,
        Workflow.class,
        WorkflowStage.class,
        WorkflowTask.class,
        CreatePipelineData.class,
        WorkerAddPipelineContainerLogMessage.class,
        WorkerGetStatusMessage.class,
        WorkerInterruptPipelineMessage.class,
        WorkerSchedulePipelineMessage.class,
        WorkerSetPipelineStatusMessage.class,
        WorkerStatusMessage.class,
})
public class RegistersForReflection {
}
