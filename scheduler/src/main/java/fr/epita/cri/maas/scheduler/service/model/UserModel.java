package fr.epita.cri.maas.scheduler.service.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Getter
@Entity
@NoArgsConstructor
public class UserModel {
    @Id private String login;
    @Setter private String displayName;
    @Setter private boolean admin;
    @OneToMany private List<TenantModel> allowedTenants;
    @OneToMany(mappedBy = "user", cascade = CascadeType.REMOVE) private List<UserTokenModel> tokens;

    public UserModel(String login, String displayName, boolean admin, List<TenantModel> allowedTenants) {
        this.login = login;
        this.displayName = displayName;
        this.admin = admin;
        this.allowedTenants = allowedTenants;
    }
}
