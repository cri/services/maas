package fr.epita.cri.maas.scheduler.service.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class WorkerModel {
    @Id private String name;
    @Setter private String authenticationToken;
}
