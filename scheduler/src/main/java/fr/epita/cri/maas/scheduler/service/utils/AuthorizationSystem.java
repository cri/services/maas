package fr.epita.cri.maas.scheduler.service.utils;

import fr.epita.cri.maas.scheduler.service.model.TenantModel;
import fr.epita.cri.maas.scheduler.service.model.UserModel;
import io.vertx.core.http.HttpServerRequest;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.Optional;
import java.util.UUID;

@ApplicationScoped
public class AuthorizationSystem {
    public static final String X_API_TOKEN = "X-Api-Token";
    public static final String X_USER_TOKEN = "X-User-Token";
    public static final String X_USER_TENANT = "X-User-Tenant";
    public static final String X_WORKER_REGISTER_TOKEN = "X-Worker-Register-Token";

    @ConfigProperty(name = "maas.worker.register-token")
    Optional<String> registerToken;

    @Inject EntityManager em;

    public boolean isUser(HttpServerRequest request) {
        return request.getHeader(X_USER_TOKEN) != null && request.getHeader(X_API_TOKEN) == null;
    }

    public boolean isSelfApiToken(HttpServerRequest request, UUID token) {
        return token.toString().equals(request.getHeader(X_API_TOKEN));
    }

    @Transactional
    public TenantModel getClientTenant(HttpServerRequest request) {
        try {
            String userToken = request.getHeader(X_USER_TOKEN);
            String userTenant = request.getHeader(X_USER_TENANT);
            String apiToken = request.getHeader(X_API_TOKEN);
            if (userToken != null && userTenant != null) {
                UUID token = UUID.fromString(userToken);
                UserModel userModel = em.createQuery("select ut.user from UserTokenModel ut where ut.token = :token", UserModel.class)
                        .setParameter("token", token)
                        .getResultList()
                        .stream()
                        .findAny()
                        .orElse(null);
                if (userModel != null) {
                    return userModel.getAllowedTenants().stream()
                            .filter(t -> t.getName().equals(userTenant))
                            .findAny()
                            .orElse(null);
                }
            } else if (apiToken != null) {
                UUID token = UUID.fromString(apiToken);
                return em.createQuery("select at.tenant from ApiTokenModel at where at.token = :token", TenantModel.class)
                        .setParameter("token", token)
                        .getResultList()
                        .stream()
                        .findAny()
                        .orElse(null);
            }
        } catch (IllegalArgumentException ignore) {

        }
        return null;
    }

    @Transactional
    public UserModel getClientUserModel(HttpServerRequest request) {
        try {
            String userToken = request.getHeader(X_USER_TOKEN);
            if (userToken != null) {
                UUID token = UUID.fromString(userToken);
                UserModel userModel = em.createQuery("select ut.user from UserTokenModel ut where ut.token = :token", UserModel.class)
                        .setParameter("token", token)
                        .getResultList()
                        .stream()
                        .findAny()
                        .orElse(null);
                return userModel;
            }
        } catch (IllegalArgumentException ignore) {

        }
        return null;
    }

    public boolean isClientAdmin(HttpServerRequest request) {
        UserModel userModel = getClientUserModel(request);
        if (userModel == null) {
            return false;
        }
        return userModel.isAdmin();
    }

    public boolean hasValidWorkerRegisterToken(HttpServerRequest request) {
        String clientRegistryToken = request.getHeader(X_WORKER_REGISTER_TOKEN);
        return registerToken.isPresent() && registerToken.get().equals(clientRegistryToken);
    }

    public String getUserTenantName(HttpServerRequest request) {
        String tenantName = request.getHeader(X_USER_TENANT);
        if (tenantName == null) {
            return "";
        }
        return tenantName;
    }
}
