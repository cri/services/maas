package fr.epita.cri.maas.scheduler.service.utils;

import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.enterprise.context.ApplicationScoped;
import java.time.Duration;

@ApplicationScoped
public class SchedulerConfig {
    @ConfigProperty(name = "maas.recovered-pipeline-reschedule-after-mins")
    int recoveredPipelineRescheduleAfterMins;

    public Duration getRecoveredPipelineRescheduleAfter() {
        return Duration.ofMinutes(recoveredPipelineRescheduleAfterMins);
    }
}
