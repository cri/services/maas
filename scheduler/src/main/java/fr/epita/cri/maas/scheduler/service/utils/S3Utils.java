package fr.epita.cri.maas.scheduler.service.utils;

import fr.epita.cri.maas.data.pipeline.Pipeline;
import lombok.NoArgsConstructor;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.S3Configuration;
import software.amazon.awssdk.services.s3.model.GetObjectRequest;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;
import software.amazon.awssdk.services.s3.presigner.S3Presigner;
import software.amazon.awssdk.services.s3.presigner.model.GetObjectPresignRequest;
import software.amazon.awssdk.services.s3.presigner.model.PresignedGetObjectRequest;
import software.amazon.awssdk.services.s3.presigner.model.PresignedPutObjectRequest;
import software.amazon.awssdk.services.s3.presigner.model.PutObjectPresignRequest;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.util.Optional;

@NoArgsConstructor
@ApplicationScoped
public class S3Utils {
    private final Region region = Region.US_WEST_1;

    @ConfigProperty(name = "maas.s3.endpoint")
    String endpoint;

    @ConfigProperty(name = "maas.s3.accesskeyfile")
    Optional<String> s3AccessKeyFile;

    @ConfigProperty(name = "maas.s3.secretkeyfile")
    Optional<String> s3SecretKeyFile;

    @ConfigProperty(name = "maas.s3.bucket")
    String bucketName;

    private String accessKey;
    private String secretKey;

    @PostConstruct
    void postConstruct() {
        try {
            this.accessKey = s3AccessKeyFile.isEmpty() ? "accessKey1" : Files.readString(Path.of(s3AccessKeyFile.get()));
            this.secretKey = s3SecretKeyFile.isEmpty() ? "verySecretKey1" : Files.readString(Path.of(s3SecretKeyFile.get()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void computePipelineResultUploadUrl(Pipeline pipeline) {
        try {
            String keyName = pipeline.getId() + ".zip";

            S3Configuration config = S3Configuration.builder()
                    .pathStyleAccessEnabled(true)
                    .checksumValidationEnabled(false)
                    .build();

            S3Presigner presigner = S3Presigner.builder()
                    .serviceConfiguration(config)
                    .region(region)
                    .endpointOverride(new URI(endpoint))
                    .credentialsProvider(() -> AwsBasicCredentials.create(accessKey, secretKey))
                    .build();

            PutObjectRequest putObjectRequest = PutObjectRequest.builder()
                    .bucket(bucketName)
                    .key(keyName)
                    .contentType("application/zip")
                    .build();

            PutObjectPresignRequest presignRequest = PutObjectPresignRequest.builder()
                    .signatureDuration(Duration.ofDays(7))
                    .putObjectRequest(putObjectRequest)
                    .build();

            PresignedPutObjectRequest presignedRequest = presigner.presignPutObject(presignRequest);

            pipeline.setResultUploadUrl(presignedRequest.url().toString());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public String uploadFileAndGetUrl(String objectName, byte[] data) {
        try {
            S3Configuration config = S3Configuration.builder()
                    .pathStyleAccessEnabled(true)
                    .checksumValidationEnabled(false)
                    .build();

            S3Client s3Client = S3Client.builder()
                    .serviceConfiguration(config)
                    .region(region)
                    .endpointOverride(new URI(endpoint))
                    .credentialsProvider(() -> AwsBasicCredentials.create(accessKey, secretKey))
                    .build();

            S3Presigner presigner = S3Presigner.builder()
                    .serviceConfiguration(config)
                    .region(region)
                    .endpointOverride(new URI(endpoint))
                    .credentialsProvider(() -> AwsBasicCredentials.create(accessKey, secretKey))
                    .build();

            //Upload the file
            s3Client.putObject(PutObjectRequest.builder().bucket(bucketName).key(objectName).build(), RequestBody.fromBytes(data));

            //Compute the request to presign
            GetObjectRequest getObjectRequest =
                    GetObjectRequest.builder()
                            .bucket(bucketName)
                            .key(objectName)
                            .build();

            //Presign the request with a 7 days validity
            GetObjectPresignRequest getObjectPresignRequest = GetObjectPresignRequest.builder()
                    .signatureDuration(Duration.ofDays(7))
                    .getObjectRequest(getObjectRequest)
                    .build();

            //Get the result
            PresignedGetObjectRequest presignedGetObjectRequest =
                    presigner.presignGetObject(getObjectPresignRequest);

            return presignedGetObjectRequest.url().toString();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getFilePresignedDownloadUrl(String objectName) {
        try {
            S3Configuration config = S3Configuration.builder()
                    .pathStyleAccessEnabled(true)
                    .checksumValidationEnabled(false)
                    .build();

            S3Presigner presigner = S3Presigner.builder()
                    .serviceConfiguration(config)
                    .region(region)
                    .endpointOverride(new URI(endpoint))
                    .credentialsProvider(() -> AwsBasicCredentials.create(accessKey, secretKey))
                    .build();

            //Compute the request to presign
            GetObjectRequest getObjectRequest = GetObjectRequest.builder()
                    .bucket(bucketName)
                    .key(objectName)
                    .build();

            //Presign the request with a 7 days validity
            GetObjectPresignRequest getObjectPresignRequest = GetObjectPresignRequest.builder()
                    .signatureDuration(Duration.ofDays(7))
                    .getObjectRequest(getObjectRequest)
                    .build();

            //Get the result
            PresignedGetObjectRequest presignedGetObjectRequest =
                    presigner.presignGetObject(getObjectPresignRequest);

            return presignedGetObjectRequest.url().toString();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return null;
    }
}
