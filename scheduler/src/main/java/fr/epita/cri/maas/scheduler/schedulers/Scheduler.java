package fr.epita.cri.maas.scheduler.schedulers;

import fr.epita.cri.maas.data.pipeline.Pipeline;
import fr.epita.cri.maas.data.pipeline.PipelineStatus;
import fr.epita.cri.maas.data.workflow.Workflow;
import fr.epita.cri.maas.scheduler.workers.Worker;
import lombok.Getter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

public abstract class Scheduler {
    private final Consumer<Pipeline> pipelineResultConsumer;
    private final Consumer<Pipeline> pipelineUpdateConsumer;
    @Getter
    private final List<Pipeline> pipelinesToRun;
    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

    public Scheduler(Consumer<Pipeline> pipelineResultConsumer, Consumer<Pipeline> pipelineUpdateConsumer) {
        this.pipelineResultConsumer = pipelineResultConsumer;
        this.pipelineUpdateConsumer = pipelineUpdateConsumer;
        this.pipelinesToRun = Collections.synchronizedList(new ArrayList<>());
        scheduler.scheduleAtFixedRate(() -> {
            try {
                tick();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }, 0, 100, TimeUnit.MILLISECONDS);
    }

    /**
     * Called to process a job
     * If the job is already running or waiting the new job is silently ignored
     */
    public void runPipeline(Pipeline pipeline) {
        //TODO Add tasks to a queue to be processed by the tick method
        if (pipelinesToRun.contains(pipeline)) {
            return; // We already have a pipeline with this uuid
        }

        pipeline.computeSteps();
        pipelinesToRun.add(pipeline);
        //tick();//FIXME
    }

    public void tick() {
        ArrayList<Pipeline> runningPipelines = new ArrayList<>();
        Iterable<Worker> workers = getWorkers();
        int totalCpuCount = 0;
        int totalMemoryMb = 0;
        for (Worker worker : workers) {
            totalCpuCount += worker.getCpuCount();
            totalMemoryMb += worker.getMemoryMb();
            runningPipelines.addAll(worker.getPipelines());
        }

        //TODO Order task from more resource demanding to less

        int usedCpuCount = 0;
        int usedMemoryMb = 0;
        for (Pipeline pipeline : runningPipelines) {
            usedCpuCount += pipeline.getWorkflow().getCpuCount();
            usedMemoryMb += pipeline.getWorkflow().getMemoryMb();
        }

        int freeCpuCount = totalCpuCount - usedCpuCount;
        int freeMemoryMb = totalMemoryMb - usedMemoryMb;

        synchronized (pipelinesToRun) {
            for (Iterator<Pipeline> it = pipelinesToRun.iterator(); it.hasNext(); ) {
                Pipeline pipeline = it.next();
                if (pipeline.getData().getStatus().isDone()) {
                    try {
                        pipelineResultConsumer.accept(pipeline);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    it.remove();
                } else if (!runningPipelines.contains(pipeline)) {
                    for (Worker worker : workers) {
                        if (worker.runPipeline(pipeline)) {
                            Workflow workflow = pipeline.getWorkflow();
                            freeCpuCount -= workflow.getCpuCount();
                            freeMemoryMb -= workflow.getMemoryMb();
                            break;
                        }
                    }
                }
            }
        }
    }

    public abstract Iterable<Worker> getWorkers();

    public void onPipelineUpdate(Pipeline pipeline) {
        pipelineUpdateConsumer.accept(pipeline);
    }
}
