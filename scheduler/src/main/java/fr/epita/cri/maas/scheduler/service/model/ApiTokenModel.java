package fr.epita.cri.maas.scheduler.service.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.Instant;
import java.util.UUID;

@Entity
@Getter
@NoArgsConstructor
public class ApiTokenModel {
    @Id private UUID token;
    @Setter @Column(nullable = false) private String description;
    @ManyToOne(optional = false) private TenantModel tenant;

    public ApiTokenModel(String description, TenantModel tenant) {
        token = UUID.randomUUID();//I am not sure hibernate use a secured random source to generate UUIDs
        this.description = description;
        this.tenant = tenant;
    }
}
