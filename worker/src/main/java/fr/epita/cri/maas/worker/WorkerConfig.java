package fr.epita.cri.maas.worker;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
public class WorkerConfig {
    private String schedulerUrl = "wss://maas.cri.epita.fr/ws/";
    private String baseUrl = "https://maas.cri.epita.fr";
    @Setter
    private String authenticationToken = "";
    private String workerRegisterToken = "";
    private String proxyHost;
    private int proxyPort;
    private boolean cleanContainers = false;
}
