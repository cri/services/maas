package fr.epita.cri.maas.worker;

import fr.epita.cri.maas.message.worker.WorkerDownstreamMessage;
import fr.epita.cri.maas.websocket.WebsocketSerializer;
import lombok.Getter;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.slf4j.Logger;

import java.net.URI;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Getter
public class StandaloneWebsocketClient extends WebSocketClient {
    public static final Logger logger = StandaloneWorker.getLogger();
    private final WorkerSystem workerSystem;
    private final ScheduledExecutorService keepaliveScheduler;

    public StandaloneWebsocketClient(URI serverUri, WorkerSystem workerSystem) {
        super(serverUri);
        this.workerSystem = workerSystem;
        this.keepaliveScheduler = Executors.newScheduledThreadPool(1);
        keepaliveScheduler.scheduleAtFixedRate(() -> {
            if (this.isOpen())
                this.sendPing();
        }, new Random().nextInt(15) + 5, 30, TimeUnit.SECONDS);
        setProxy(workerSystem.getProxy());
    }

    @Override
    public void onOpen(ServerHandshake serverHandshake) {
        logger.info("WebSocket Connected");
        send("AUTH " + workerSystem.getWorkerConfig().getAuthenticationToken());
    }

    @Override
    public void onMessage(String s) {
        logger.debug("WebSocket Received {}", s);
        WorkerDownstreamMessage workerDownstreamMessage = (WorkerDownstreamMessage) WebsocketSerializer.deserializeWebsocketMessage(s);
        workerSystem.onDownstreamMessage(workerDownstreamMessage);
    }

    @Override
    public void onClose(int i, String s, boolean b) {
        logger.info("WebSocket Closed {}", s);
    }

    @Override
    public void onError(Exception e) {
        logger.info("WebSocket Exception {}", e.toString());
    }

    @Override
    public void send(String text) {
        super.send(text);
        logger.debug("WebSocket Send {}", text);
    }
}
