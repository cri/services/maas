package fr.epita.cri.maas.worker;

import fr.epita.cri.maas.data.pipeline.Pipeline;
import fr.epita.cri.maas.data.workflow.Workflow;
import fr.epita.cri.maas.message.worker.*;
import fr.epita.cri.maas.runner.PipelineRunner;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.lang.management.ManagementFactory;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public abstract class WorkerSystem {
    private static final Logger logger = LoggerFactory.getLogger(WorkerSystem.class);
    @Getter
    private final WorkerConfig workerConfig;
    private int cpuCount;
    private int memoryMb;
    private volatile Pipeline[] coresPipelines;
    private ExecutorService pipelineExecutorService;
    @Getter
    private Proxy proxy;

    public WorkerSystem(WorkerConfig workerConfig) {
        this.workerConfig = workerConfig;
        setupProxy();
        computeCapabilities();
    }

    /**
     * Commands to remote worker
     *
     * @param workerMessage
     */
    public void onDownstreamMessage(WorkerDownstreamMessage workerMessage) {
        if (workerMessage instanceof WorkerGetStatusMessage) {
            sendUpstreamMessage(new WorkerStatusMessage(cpuCount, memoryMb, getRunningPipelines()));
            //TODO Send all tasks
        } else if (workerMessage instanceof WorkerSchedulePipelineMessage) {
            WorkerSchedulePipelineMessage workerSchedulePipelineMessage = (WorkerSchedulePipelineMessage) workerMessage;
            Pipeline pipeline = workerSchedulePipelineMessage.getPipeline();
            Workflow workflow = pipeline.getWorkflow();

            int cpuCoresCount = workflow.getCpuCount();
            Collection<Integer> cores = lockCpuCores(pipeline, cpuCoresCount);
            pipeline.getData().setCpuCores(cores);
            sendPipelineStatus(pipeline);

            pipelineExecutorService.submit(() -> {
                PipelineRunner pipelineRunner = new PipelineRunner(pipeline,
                        pipelineFolder -> uploadPipelineState(pipeline, pipelineFolder),
                        pipelineStatus -> sendPipelineStatus(pipeline),
                        pipelineLogLine -> sendLogLine(pipeline, pipelineLogLine.getContainerUuid(), pipelineLogLine.getLogLine()));
                pipelineRunner.runPipeline(pipeline.getSourceDataUrl(), getProxy());

                pipeline.getData().getCpuCores().clear();
                unlockCpuCores(pipeline);
                sendPipelineStatus(pipeline);
            });
        }
    }

    private boolean uploadPipelineState(Pipeline pipeline, File pipelineFolder) {
        File zipFile = new File(pipelineFolder, "full.zip");

        ArrayList<String> filesList = new ArrayList<>();

        listFiles(filesList, pipelineFolder);
        int baseFilenameLen = pipelineFolder.getAbsolutePath().length();

        //Create a zip archive containing all the files in fht pipeline folder
        try (ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(zipFile))) {
            byte[] buff = new byte[1024];
            int readedLen;
            for (String fileName : filesList) {
                fileName = fileName.substring(baseFilenameLen + 1);
                zos.putNextEntry(new ZipEntry(fileName));
                File targetFile = new File(pipelineFolder, fileName);
                try (FileInputStream fis = new FileInputStream(targetFile)) {
                    while ((readedLen = fis.read(buff)) >= 0) {
                        zos.write(buff, 0, readedLen);
                    }
                }
                zos.closeEntry();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Upload the archive
        boolean success = uploadFile(zipFile, pipeline.getResultUploadUrl());
        zipFile.delete();
        return success;
    }

    private void listFiles(ArrayList<String> filesList, File file) {
        if (file.isDirectory()) {
            for (File subfile : file.listFiles()) {
                listFiles(filesList, subfile);
            }
        } else {
            filesList.add(file.getAbsolutePath());
        }
    }

    private boolean uploadFile(File outputFile, String uploadUrl) {
        try {
            URL url = new URL(uploadUrl);

            Proxy proxy = getProxy();

            HttpURLConnection connection = (HttpURLConnection) url.openConnection(proxy);
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/zip");
            connection.setRequestMethod("PUT");
            OutputStream outputStream = connection.getOutputStream();
            try (FileInputStream fis = new FileInputStream(outputFile)) {
                byte[] buffer = new byte[1024];
                int readedLen;
                while ((readedLen = fis.read(buffer)) >= 0) {
                    outputStream.write(buffer, 0, readedLen);
                }
            }
            outputStream.close();

            int responseCode = connection.getResponseCode();
            if (responseCode != 200) {
                logger.error("Unable to upload file, status code {} when uploading to {}", responseCode, uploadUrl);
            } else {
                return true;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    private void setupProxy() {
        proxy = Proxy.NO_PROXY;
        String proxyHost = workerConfig.getProxyHost();
        int proxyPort = workerConfig.getProxyPort();
        if (proxyHost != null && proxyPort != 0) {
            proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxyHost, proxyPort));
        }
    }

    public void sendPipelineStatus(Pipeline pipeline) {
        sendUpstreamMessage(new WorkerSetPipelineStatusMessage(pipeline));
    }

    public void sendLogLine(Pipeline pipeline, UUID containerUuid, String logLine) {
        sendUpstreamMessage(new WorkerAddPipelineContainerLogMessage(pipeline.getId(), containerUuid, logLine));
    }

    private long getSecs() {
        return (System.currentTimeMillis() / 1000L) % 60;
    }

    public abstract void sendUpstreamMessage(WorkerUpstreamMessage workerUpstreamMessage);

    protected void computeCapabilities() {
        cpuCount = Runtime.getRuntime().availableProcessors(); // Returns threads count
        memoryMb = (int) (((com.sun.management.OperatingSystemMXBean) ManagementFactory
                .getOperatingSystemMXBean()).getTotalPhysicalMemorySize() / 1024L / 1024L);

        coresPipelines = new Pipeline[cpuCount];
        pipelineExecutorService = Executors.newFixedThreadPool(cpuCount);
    }

    protected Collection<Integer> lockCpuCores(Pipeline pipeline, int cpuCoresCount) {
        ArrayList<Integer> cpuCores = new ArrayList<>(cpuCoresCount);
        for (int i = 0; i < coresPipelines.length; i++) {
            if (coresPipelines[i] == null) {
                coresPipelines[i] = pipeline;
                cpuCores.add(i);
                if (cpuCores.size() >= cpuCoresCount) {
                    break;
                }
            }
        }
        return cpuCores;
    }

    protected void unlockCpuCores(Pipeline pipeline) {
        for (int i = 0; i < coresPipelines.length; i++) {
            if (coresPipelines[i] == pipeline) {
                coresPipelines[i] = null;
            }
        }
    }

    protected int getFreeCpuCoreCount() {
        int count = 0;
        for (int i = 0; i < coresPipelines.length; i++) {
            if (coresPipelines[i] == null) {
                count++;
            }
        }
        return count;
    }

    private List<UUID> getRunningPipelines() {
        return Arrays.stream(coresPipelines)
                .filter(Objects::nonNull)
                .map(Pipeline::getId)
                .distinct()
                .collect(Collectors.toList());
    }
}
