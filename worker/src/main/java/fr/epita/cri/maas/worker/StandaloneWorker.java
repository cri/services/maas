package fr.epita.cri.maas.worker;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.epita.cri.maas.message.worker.WorkerUpstreamMessage;
import fr.epita.cri.maas.util.SerializerUtils;
import fr.epita.cri.maas.websocket.WebsocketSerializer;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class StandaloneWorker {
    @Getter
    private static final Logger logger = LoggerFactory.getLogger(WorkerSystem.class);
    private final WorkerSystem workerSystem;
    private final LinkedBlockingQueue<String> messagesToSend = new LinkedBlockingQueue<>(10);
    private final WorkerConfig workerConfig = loadConfig();

    public StandaloneWorker() {
        workerSystem = new WorkerSystem(workerConfig) {
            @Override
            public void sendUpstreamMessage(WorkerUpstreamMessage workerUpstreamMessage) {
                String serializedMessage = WebsocketSerializer.serializeWebsocketMessage(workerUpstreamMessage);
                try {
                    messagesToSend.put(serializedMessage);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        if (workerConfig.isCleanContainers()) {
            cleanContainers();
        }
        getWorkerToken();
        startWebsocketConnection();
    }

    private void cleanContainers() {
        //Dirty way of getting rid of leftover running containers
        try {
            Process process = Runtime.getRuntime().exec(new String[]{"docker", "ps"});
            process.waitFor();
            try (BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream()))) {
                br.readLine();// Read and discard the header ling
                String line = null;
                while ((line = br.readLine()) != null) {
                    String containerHash = line.substring(0, line.indexOf(' '));
                    Runtime.getRuntime().exec(new String[]{"docker", "rm", "-f", containerHash}).waitFor(); // We could remove all containers at once
                }
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void getWorkerToken() {
        while (workerConfig.getAuthenticationToken() == null || workerConfig.getAuthenticationToken().isEmpty()) {
            try {
                //TODO Find a better way to do this
                String workerName = Files.readString(new File("/proc/sys/kernel/hostname").toPath());

                URL url = new URL(workerConfig.getBaseUrl() + "/worker/" + workerName);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection(workerSystem.getProxy());
                httpURLConnection.setRequestProperty("X-Worker-Register-Token", workerConfig.getWorkerRegisterToken());
                httpURLConnection.setRequestMethod("PUT");

                if (httpURLConnection.getResponseCode() != 200) {
                    throw new RuntimeException("Response code " + httpURLConnection.getResponseCode() + " for url " + url);
                }

                try (InputStream is = httpURLConnection.getInputStream();
                     ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
                    byte[] buff = new byte[4096];
                    int readedLen;

                    while ((readedLen = is.read(buff)) > -1) {
                        baos.write(buff, 0, readedLen);
                    }

                    workerConfig.setAuthenticationToken(baos.toString(StandardCharsets.UTF_8));
                }
            } catch (Exception e) {
                System.err.println("Failed to get worker token");
                e.printStackTrace();

                //The token might be invalid, or the scheduler unavailable, thus we wait, hoping for the scheduler to become available
                System.err.println("Waiting 60s before retry");
                try {
                    Thread.sleep(60000);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public void startWebsocketConnection() {
        new Thread(() -> {
            int connectionFailure = 0;
            try {
                URI uri = new URI(workerConfig.getSchedulerUrl());
                StandaloneWebsocketClient standaloneWebsocketClient = new StandaloneWebsocketClient(uri, workerSystem);
                standaloneWebsocketClient.connectBlocking();

                while (true) {//TODO while(running)
                    String message = messagesToSend.poll(1, TimeUnit.SECONDS);

                    while (!standaloneWebsocketClient.isOpen()) {
                        logger.info("Reconnecting...");
                        if (connectionFailure > 0) {
                            Thread.sleep(Math.min(10, connectionFailure) * 1000L);
                        }
                        standaloneWebsocketClient.reconnectBlocking();
                        connectionFailure++;
                    }
                    connectionFailure = 0;

                    if (message != null) {
                        standaloneWebsocketClient.send(message);
                    }
                }
            } catch (URISyntaxException | InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
    }

    private WorkerConfig loadConfig() {
        File configFile = new File("config.yaml");
        if (!configFile.exists()) {
            logger.warn("Unable to find config file at " + configFile.getAbsolutePath());
        } else {
            ObjectMapper yamlObjectMapper = SerializerUtils.getYamlObjectMapper();
            try {
                return yamlObjectMapper.readValue(configFile, WorkerConfig.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return new WorkerConfig();
    }

    public static void main(String[] args) {
        new StandaloneWorker();
    }
}
