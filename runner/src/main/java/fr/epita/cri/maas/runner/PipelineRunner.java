package fr.epita.cri.maas.runner;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.epita.cri.maas.data.pipeline.*;
import fr.epita.cri.maas.util.FileUtils;
import fr.epita.cri.maas.util.SerializerUtils;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.Proxy;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class PipelineRunner {
    private static final Logger logger = LoggerFactory.getLogger(PipelineRunner.class);
    private final static UUID metaContainerId = new UUID(0, 0);
    private final Pipeline pipeline;
    private final Function<File, Boolean> uploadStateConsumer;
    private final Consumer<PipelineStatus> statusChangeConsumer;
    private final Consumer<PipelineLogLine> logLineConsumer;
    private File pipelineFolder;
    private File mountsFolder;
    private File containersFolder;
    @Getter
    private File inputFile;
    private File outputFile;
    private long startTimestamp;
    private long endTimestamp;
    private boolean deleteOnExit = true;
    private final ConcurrentLinkedQueue<PipelineLogLine> logLines = new ConcurrentLinkedQueue<>();

    public void runPipeline(String inputFileUrl, Proxy proxy) {
        String coresListStr = pipeline.getData().getCpuCores().stream().map(Object::toString).collect(Collectors.joining(", "));
        logger.info("[{}] Starting cores [{}] memory {}MB", pipeline.getId(), coresListStr, pipeline.getWorkflow().getMemoryMb());
        long totalStartTimestamp = System.currentTimeMillis();

        setupFiles();

        boolean downloadFailed = false;
        if (inputFileUrl != null) {
            downloadFailed = true;
            for (int i = 0; i < 5; i++) {
                if (FileUtils.downloadFile(inputFileUrl, inputFile, new HashMap<>(), proxy)) {
                    downloadFailed = false;
                    break;
                } else {
                    // Wait a bit before trying to download the file again
                    try {
                        Thread.sleep(10000); //10s
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            if (downloadFailed) {
                logger.error("[{}] Failed to download input file from {}", pipeline.getId(), inputFileUrl);
                onContainerLog(metaContainerId, "Failed to download from " + inputFileUrl);
                setPipelineStatus(PipelineStatus.FAILED);
            }
        }

        if (!downloadFailed) {
            runInternal();
        }

        cleanupFiles();

        PipelineData pipelineData = pipeline.getData();
        if (!pipelineData.getStatus().isDone()) { //Avoid overriding FAILED pipeline status
            setPipelineStatus(PipelineStatus.ENDED);
        }

        long totalEndTimestamp = System.currentTimeMillis();
        logger.info("[{}] {} in {}ms (total {}ms)", pipeline.getId(), pipelineData.getStatus(), endTimestamp - startTimestamp, totalEndTimestamp - totalStartTimestamp);
    }

    /**
     * Method used by the CLI to run the pipeline locally
     *
     * @param inputData    byte array of the input data
     * @param deleteOnExit If we should cleanup on exit
     */
    public void runPipeline(byte[] inputData, boolean deleteOnExit) {
        this.deleteOnExit = deleteOnExit;

        setupFiles();

        try {
            Files.write(inputFile.toPath(), inputData);
        } catch (IOException e) {
            e.printStackTrace();
        }

        runInternal();
        cleanupFiles();
    }

    private void runInternal() {
        try {
            if (!inputFile.exists()) {
                Files.write(inputFile.toPath(), new byte[]{});
            }
            if (!outputFile.exists()) {
                Files.write(outputFile.toPath(), new byte[]{});
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        PipelineData pipelineData = pipeline.getData();
        setPipelineStatus(PipelineStatus.SETUP);

        for (String image : pipeline.getImages()) {
            if (!downloadDockerImageIfNotPresent(image)) {
                onContainerLog(metaContainerId, "Unable to download docker image " + image);
                logger.error("[{}] Unable to download docker image " + image, pipeline.getId());
                setPipelineStatus(PipelineStatus.FAILED);
                return;
            }
        }

        startTimestamp = System.currentTimeMillis();

        setPipelineStatus(PipelineStatus.RUNNING);
        int totalMemoryMb = pipeline.getWorkflow().getMemoryMb();
        stages:
        for (PipelineStage stage : pipelineData.getStages()) {
            long stageStartTimestamp = System.currentTimeMillis();
            ArrayList<ContainerRunner> containerRunners = new ArrayList<>();
            for (PipelineContainer pipelineContainer : stage.getContainers()) {
                File containerFolder = new File(containersFolder, pipelineContainer.getId().toString());
                containerFolder.mkdirs();
                ContainerRunner containerRunner = new ContainerRunner(
                        pipelineContainer.getTask().getCpus(),
                        pipelineContainer.getMemoryMb(),
                        inputFile,
                        outputFile,
                        mountsFolder,
                        containerFolder,
                        pipelineContainer,
                        line -> onContainerLog(pipelineContainer.getId(), line)
                );
                containerRunners.add(containerRunner);
            }

            while (containerRunners.stream()
                    .anyMatch(ContainerRunner::isWaitingToStart)) {

                List<Integer> usedCpus = containerRunners.stream()
                        .filter(ContainerRunner::isRunning)
                        .flatMap(c -> c.getCpuCores().stream())
                        .collect(Collectors.toList());

                List<Integer> availableCpus = pipeline.getData().getCpuCores().stream()
                        .filter(cpu -> !usedCpus.contains(cpu))
                        .collect(Collectors.toList());

                int usedMemory = containerRunners.stream()
                        .filter(ContainerRunner::isRunning)
                        .mapToInt(ContainerRunner::getMemoryMb)
                        .sum();

                int freeMemory = totalMemoryMb - usedMemory;
                ContainerRunner containerRunner = containerRunners.stream()
                        .filter(ContainerRunner::isWaitingToStart)
                        .filter(c -> c.getCpuCount() <= availableCpus.size())
                        .filter(c -> c.getMemoryMb() <= freeMemory)
                        .findAny().orElse(null);

                containerRunners.forEach(ContainerRunner::tick);
                if (containerRunner != null) {
                    long pipelineTimeoutExpiration = startTimestamp + pipeline.getWorkflow().getTimeout() * 1000L;
                    containerRunner.setCpuCores(availableCpus.subList(0, containerRunner.getCpuCount()));
                    containerRunner.launch(pipelineTimeoutExpiration);
                } else {
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            containerRunners.forEach(ContainerRunner::waitForResult);
            stage.setExecutionTime(System.currentTimeMillis() - stageStartTimestamp);

            //TODO Improvement: Process tasks that might fail first and interrupt other tasks on failure
            for (ContainerRunner containerRunner : containerRunners) {
                PipelineContainer pipelineContainer = containerRunner.getPipelineContainer();
                final int exitCode = pipelineContainer.getExitCode();
                if (exitCode != 0 && pipelineContainer.getTask().isInterruptOnFailure()) {
                    //We have a task that should have completed successfully but did not
                    onContainerLog(pipelineContainer.getId(), "Container exited with " + exitCode);
                    logger.info("[{}] Pipeline failed with exit code {} after {}ms", pipeline.getId(), exitCode,
                            pipelineContainer.getExecutionTime());
                    setPipelineStatus(PipelineStatus.FAILED);
                    break stages;
                }
            }
        }

        endTimestamp = System.currentTimeMillis();
    }

    private void cleanupFiles() {
        //Cleanup before uploading final result
        if (deleteOnExit) {
            FileUtils.deleteDir(mountsFolder);
            inputFile.delete();
        }

        writeGlobalLogFile(new File(pipelineFolder, "logs.txt"));

        //Create execution report file
        serializePipelineSummary(new File(pipelineFolder, "summary.json"));

        //Upload final result
        boolean uploadSuccess = false;
        for (int i = 0; i < 5; i++) {
            if (uploadStateConsumer.apply(pipelineFolder)) {
                uploadSuccess = true;
                break;
            }
            try {
                Thread.sleep(10000); //10s
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if (!uploadSuccess) {
            onContainerLog(metaContainerId, "Unable to upload final result");
            logger.error("[{}] Unable to upload final result", pipeline.getId());
            setPipelineStatus(PipelineStatus.FAILED);
        }

        FileUtils.deleteDir(pipelineFolder);
    }

    private void setPipelineStatus(PipelineStatus status) {
        PipelineData pipelineData = pipeline.getData();
        pipelineData.setStatus(status);
        statusChangeConsumer.accept(status);
    }

    private void setupFiles() {
        String pipelineFolderName = "maas_";
        String workflowName = pipeline.getWorkflow().getName();
        if (workflowName != null) {
            pipelineFolderName += workflowName.replaceAll("[^A-Za-z0-9]+", "-") + "_";
        }
        pipelineFolderName += pipeline.getId();

        pipelineFolder = new File("/tmp/" + pipelineFolderName);
        mountsFolder = new File(pipelineFolder, "mounts");
        containersFolder = new File(pipelineFolder, "containers");
        inputFile = new File(pipelineFolder, "in.bin");
        outputFile = new File(pipelineFolder, "out.bin");

        mountsFolder.mkdirs();
        containersFolder.mkdirs();
    }

    private void writeGlobalLogFile(File globalLogFile) {
        try (FileOutputStream fos = new FileOutputStream(globalLogFile)) {
            byte[] lineSeparator = System.lineSeparator().getBytes(StandardCharsets.UTF_8);
            for (PipelineLogLine logLine : logLines) {
                fos.write(logLine.toString().getBytes(StandardCharsets.UTF_8));
                fos.write(lineSeparator);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void serializePipelineSummary(File outputFile) {
        ObjectMapper objectMapper = SerializerUtils.getJsonObjectMapper();

        PipelineSummary pipelineSummary = new PipelineSummary(pipeline);

        try {
            Files.writeString(outputFile.toPath(), objectMapper.writeValueAsString(pipelineSummary));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean downloadDockerImageIfNotPresent(String image) {
        int downloadExpirationSecs = 60 * 10; // Interrupt download and fail after 10 minutes
        try {
            Process checkImageProcess = Runtime.getRuntime().exec("docker images -q " + image);
            checkImageProcess.waitFor();

            //If the command print something on stdout we know we have the image locally, otherwise download the image
            if (checkImageProcess.getInputStream().available() == 0) {
                onContainerLog(metaContainerId, "Downloading " + image);
                Process downloadImageProcess = Runtime.getRuntime().exec("docker pull " + image);
                if (!downloadImageProcess.waitFor(downloadExpirationSecs, TimeUnit.SECONDS)) {
                    logger.error("[{}] Timed out downloading Docker image {} in {}s", pipeline.getId(), image, downloadExpirationSecs);
                    onContainerLog(metaContainerId, "Failed to download docker image in time (" + downloadExpirationSecs + "s) " + image);
                    downloadImageProcess.destroyForcibly();
                    return false;
                }
                int exitValue = downloadImageProcess.exitValue();
                if (exitValue != 0) {
                    logger.error("[{}] Docker pull of {} returned {}", pipeline.getId(), image, exitValue);
                    onContainerLog(metaContainerId, "Docker pull returned exit code " + exitValue + " trying to download image " + image);
                    return false;
                }
            } else {
                onContainerLog(metaContainerId, "Found " + image + " locally");
            }
            return true;
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void onContainerLog(UUID containerId, String logLine) {
        PipelineLogLine pipelineLogLine = new PipelineLogLine(containerId, logLine);
        logLines.add(pipelineLogLine);
        logLineConsumer.accept(pipelineLogLine);
    }
}
