package fr.epita.cri.maas.runner;

import fr.epita.cri.maas.data.pipeline.PipelineContainer;
import fr.epita.cri.maas.data.workflow.WorkflowTask;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class ContainerRunner {
    public static final int MAX_LOG_CHARACTERS = 10000000;
    @Getter
    private final int cpuCount;
    @Getter
    @Setter
    private Collection<Integer> cpuCores;
    @Getter
    private final int memoryMb;
    private final File inputFilePath;
    private final File outputFilePath;
    private final File mountsFolder;
    private final File containerFolder;
    @Getter
    private final PipelineContainer pipelineContainer;
    private final Consumer<String> logConsumer;
    private Process process;
    private long startTimestamp;
    private long timeoutExpiration;
    @Getter
    private boolean waitingToStart = true;

    public boolean launch(long pipelineTimeoutExpiration) {
        waitingToStart = false;
        String[] commandArr = getCommand().toArray(new String[0]);
        String command = String.join(" ", commandArr);
        if (logConsumer != null) {
            logConsumer.accept(command);
        }
        for (String commandPart : commandArr) {
            if (commandPart == null) {
                if (logConsumer != null) {
                    logConsumer.accept("Having null part in command, aborting");
                }
                pipelineContainer.setExitCode(127);
                return false;
            }
        }
        try {
            process = Runtime.getRuntime().exec(commandArr);
            if (logConsumer != null) {
                consumeStream(process.getInputStream(), new File(containerFolder, "stdout.log"));
                consumeStream(process.getErrorStream(), new File(containerFolder, "stderr.log"));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        startTimestamp = System.currentTimeMillis();
        timeoutExpiration = startTimestamp + pipelineContainer.getTask().getTimeout() * 1000L;
        if (pipelineTimeoutExpiration < timeoutExpiration) {
            timeoutExpiration = pipelineTimeoutExpiration;
        }
        return true;
    }

    /**
     * Used to tick process regularly and enforce timeouts
     */
    public void tick() {
        if (process == null) {
            return;
        }

        if (!process.isAlive()) {//Process exited by himself
            long executionTime = System.currentTimeMillis() - startTimestamp;//Precision by the frequency of ticks (~10ms)
            pipelineContainer.setExecutionTime(executionTime);
            pipelineContainer.setExitCode(process.exitValue());
            process = null;
            return;
        }

        //TODO Ensure the machine is not frozen when working with timeout
        long expireIn = timeoutExpiration - System.currentTimeMillis();
        if (expireIn <= 0) {
            interruptProcess();
        }
    }

    /**
     * The final wait, will return after process exit or timeout expiration
     */
    public void waitForResult() {
        if (process == null) {
            return;
        }

        long expireIn = timeoutExpiration - System.currentTimeMillis();
        try {
            process.waitFor(expireIn, TimeUnit.MILLISECONDS);
        } catch (InterruptedException ignore) {
        }
        tick();//Will interrupt the process if running for too long
    }

    public boolean isRunning() {
        return process != null && process.isAlive();
    }

    public boolean hasEnded() {
        return process != null && !isRunning();
    }

    private void interruptProcess() {
        String killCommand = "docker rm -f " + pipelineContainer.getId().toString();
        try {
            Runtime.getRuntime().exec(killCommand).waitFor();
            process.waitFor();
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        }
        pipelineContainer.setExitCode(124); // Exit code 124 -> timeout
        long executionTime = System.currentTimeMillis() - startTimestamp;
        pipelineContainer.setExecutionTime(executionTime);
        logConsumer.accept("Interrupted after " + (executionTime / 1000L) + "s (Timeout expiration)");
    }

    private ArrayList<String> getCommand() {
        ArrayList<String> command = new ArrayList<>();
        command.add("docker");
        command.add("run");

        // command.add("--detach"); // We may want to use that -> See on how to fetch the logs
        command.add("--rm"); // Destroy the container automatically on exit

        command.add("--name");
        command.add(pipelineContainer.getId().toString());//Use a random uuid as a name

        command.add("--cpuset-cpus");//Limit to the desired cpus cores
        command.add(cpuCores.stream()
                .map(String::valueOf)
                .collect(Collectors.joining(",")));

        command.add("--memory");//Limit memory
        command.add(memoryMb + "m");

        command.add("--network=none");//Disable network

        command.add("--sysctl=net.ipv6.conf.all.disable_ipv6=0");// Keep IPv6 enabled to have loopback address

        WorkflowTask task = pipelineContainer.getTask();
        if (task.getInputFilePath() != null) {
            //Mount input file in read only mode
            command.add("-v");
            command.add(inputFilePath + ":" + task.getInputFilePath() + ":ro");
        }

        if (task.getOutputFilePath() != null) {
            //Mount output file
            command.add("-v");
            command.add(outputFilePath + ":" + task.getOutputFilePath());
        }

        if (task.getMounts() != null) {
            //Mount shared containers mounts
            for (String mount : task.getMounts()) {
                String hostMountPath = mount.substring(0, mount.indexOf(':'));
                File hostMountFile = new File(mountsFolder, hostMountPath);
                if (Files.isSymbolicLink(hostMountFile.toPath())) {
                    continue;//We do not mount symbolic links because they might provide access to critical parts of the system
                }

                if (!hostMountFile.exists()) {
                    hostMountFile.mkdirs();
                }

                command.add("-v");
                command.add(hostMountFile.getAbsolutePath() + mount.substring(mount.indexOf(':')));
            }

            for (String hostMount : task.getHostMounts()) {
                if (!Files.exists(Path.of(hostMount.substring(0, hostMount.indexOf(':'))))) {
                    continue;
                }
                command.add("-v");
                command.add(hostMount);
            }
        }

        if (pipelineContainer.getTask().getWorkdir() != null) {
            command.add("-w");
            command.add(pipelineContainer.getTask().getWorkdir());
        }

        for (Map.Entry<String, String> entry : pipelineContainer.getVariables().entrySet()) {
            String variableName = entry.getKey();
            String variableValue = entry.getValue();
            command.add("-e");
            command.add(variableName + "=" + variableValue);
        }

        for (String capability : task.getCapabilities()) {
            command.add("--cap-add");
            command.add(capability);
        }

        String[] commands = task.getCommands();
        if (commands != null) {
            //Generate commands file in logs folder
            File commandsScript = new File(containerFolder, "commands");

            //Print commands in the file
            try (PrintStream ps = new PrintStream(new FileOutputStream(commandsScript))) {
                for (String cmd : commands) {
                    ps.println(cmd);
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            //Add execution permission
            addExecutionPermission(commandsScript);

            //Mount the script in read only mode
            command.add("-v");
            command.add(commandsScript.getAbsolutePath() + ":/commands:ro");
        }

        command.add(task.getImage());

        if (commands != null) {
            String commandInterpreter = task.getCommandInterpreter();
            if (commandInterpreter != null && !commandInterpreter.isEmpty()) {
                command.add(commandInterpreter);
                command.add("-c");//Should we really hardcode this option ?
                command.add("/commands");
            }
        } else if (task.getCommand() != null) {
            command.addAll(Arrays.asList(task.getCommand()));
        }

        return command;
    }

    private void addExecutionPermission(File commandsScript) {
        try {
            Set<PosixFilePermission> ownerWritable = PosixFilePermissions.fromString("rwxr-xr-x");
            Files.setPosixFilePermissions(commandsScript.toPath(), ownerWritable);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void consumeStream(InputStream inputStream, File outputFile) {
        new Thread(() -> {
            int readedChars = 0;
            try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                 PrintWriter printWriter = new PrintWriter(new FileWriter(outputFile))) {
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    readedChars += line.length();
                    if (readedChars > MAX_LOG_CHARACTERS) {
                        break;
                    }
                    printWriter.write(line + System.lineSeparator());
                    logConsumer.accept(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }
}
