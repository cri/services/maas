package fr.epita.cri.maas.runner;

import fr.epita.cri.maas.data.pipeline.Pipeline;
import fr.epita.cri.maas.data.pipeline.PipelineData;
import fr.epita.cri.maas.data.pipeline.PipelineStage;
import fr.epita.cri.maas.data.pipeline.PipelineStatus;
import fr.epita.cri.maas.data.workflow.Workflow;
import lombok.Getter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

@Getter
public class PipelineSummary {
    private UUID id;
    private ArrayList<PipelineStage> stages = new ArrayList<>();
    private PipelineStatus status;
    private Workflow workflow;
    private HashMap<String, String> variables;
    private String sourceDataUrl;
    private String statusCallbackUrl;

    public PipelineSummary(Pipeline pipeline) {
        this.id = pipeline.getId();
        this.stages = pipeline.getData().getStages();
        this.status = pipeline.getData().getStatus();
        this.workflow = pipeline.getWorkflow();
        this.variables = pipeline.getVariables();
        this.sourceDataUrl = pipeline.getSourceDataUrl();
        this.statusCallbackUrl = pipeline.getStatusCallbackUrl();
    }
}
