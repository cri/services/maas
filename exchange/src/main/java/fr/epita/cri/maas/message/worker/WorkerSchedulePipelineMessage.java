package fr.epita.cri.maas.message.worker;

import fr.epita.cri.maas.data.pipeline.Pipeline;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class WorkerSchedulePipelineMessage extends WorkerDownstreamMessage {
    private Pipeline pipeline;
}
