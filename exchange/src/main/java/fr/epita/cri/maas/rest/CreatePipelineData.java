package fr.epita.cri.maas.rest;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashMap;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class CreatePipelineData {
    private String workflowYaml;
    private HashMap<String, String> variables = new HashMap<>();
    @Setter
    private String sourceDataUrl;
    private byte[] sourceData;
    private String statusCallbackUrl;
}
