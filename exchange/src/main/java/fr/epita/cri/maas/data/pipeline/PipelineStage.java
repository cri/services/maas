package fr.epita.cri.maas.data.pipeline;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;

@Getter
@NoArgsConstructor
public class PipelineStage {
    private ArrayList<PipelineContainer> containers = new ArrayList<>();
    @Setter
    private long executionTime;
}
