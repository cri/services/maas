package fr.epita.cri.maas.data.pipeline;

import fr.epita.cri.maas.data.workflow.WorkflowTask;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashMap;
import java.util.UUID;

@Getter
@NoArgsConstructor
public class PipelineContainer {
    private UUID id = UUID.randomUUID();
    private WorkflowTask task;
    private HashMap<String, String> variables;
    private int memoryMb;
    @Setter
    private int exitCode;
    @Setter
    private long executionTime;

    public PipelineContainer(WorkflowTask task, HashMap<String, String> variables, int memoryMb) {
        this.task = task;
        this.variables = variables;
        this.memoryMb = memoryMb;
    }
}
