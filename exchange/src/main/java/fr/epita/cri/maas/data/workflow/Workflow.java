package fr.epita.cri.maas.data.workflow;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.List;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Workflow {
    private String name;
    private HashMap<String, String> variables = new HashMap<>();
    private List<WorkflowStage> stages;
    private int cpuCount = 1;
    private int memoryMb = 1024;
    private int timeout = 60;//Default timeout of 60s

    public void sanitize() throws IllegalArgumentException {
        //TODO Check env variables len ?
        if (cpuCount < 1 || cpuCount > 4) {//TODO Global CPU limit
            throw new IllegalArgumentException("Invalid cpu count");
        }
        if (memoryMb < 1 || memoryMb > 10 * 1024) {//TODO Global Memory limit
            throw new IllegalArgumentException("Invalid memory amount");
        }
        if (timeout < 1 || timeout > 2 * 60 * 60) {//TODO Global timeout limit
            throw new IllegalArgumentException("Invalid workflow timeout");
        }

        if (variables == null) {
            variables = new HashMap<>();
        }

        if (stages == null || stages.size() == 0) {
            throw new IllegalArgumentException("Workflow must have at least one stage");
        }

        for (WorkflowStage workflowStage : stages) {
            workflowStage.sanitize(this);
        }
    }
}
