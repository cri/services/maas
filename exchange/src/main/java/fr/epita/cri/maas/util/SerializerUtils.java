package fr.epita.cri.maas.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.Getter;

public class SerializerUtils {
    @Getter
    private static final ObjectMapper yamlObjectMapper;
    @Getter
    private static final ObjectMapper jsonObjectMapper = new ObjectMapper();

    static {
        yamlObjectMapper = new ObjectMapper(new YAMLFactory());
        yamlObjectMapper.findAndRegisterModules(); //Needed for optional modules
        yamlObjectMapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);

        jsonObjectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        jsonObjectMapper.registerModule(new JavaTimeModule());
    }
}
