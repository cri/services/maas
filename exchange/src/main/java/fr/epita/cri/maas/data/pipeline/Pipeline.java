package fr.epita.cri.maas.data.pipeline;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fr.epita.cri.maas.data.workflow.Workflow;
import fr.epita.cri.maas.data.workflow.WorkflowStage;
import fr.epita.cri.maas.data.workflow.WorkflowTask;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.stream.Collectors;

@Getter
@NoArgsConstructor
public class Pipeline {
    private UUID id;
    private String tenant;
    private Workflow workflow;
    private HashMap<String, String> variables;
    private String sourceDataUrl;
    private String statusCallbackUrl;
    @Setter private String resultUploadUrl;
    @Setter private PipelineData data;
    @JsonIgnore private ConcurrentLinkedQueue<PipelineLogLine> logLines = new ConcurrentLinkedQueue<>();

    public Pipeline(UUID id,
                    String tenant,
                    Workflow workflow,
                    HashMap<String, String> variables,
                    String sourceDataUrl,
                    String statusCallbackUrl,
                    PipelineData data) {
        this.id = id;
        this.tenant = tenant;
        this.workflow = workflow;
        this.variables = variables;
        this.sourceDataUrl = sourceDataUrl;
        this.statusCallbackUrl = statusCallbackUrl;
        this.data = data;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Pipeline) {
            Pipeline pipeline = (Pipeline) obj; // Oui, je sais, c'est old school
            return id.equals(pipeline.id);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    public void computeSteps() {
        workflow.getVariables().forEach(variables::putIfAbsent);

        for (WorkflowStage stage : workflow.getStages()) {
            PipelineStage pipelineStage = new PipelineStage();
            int totalMemoryMb = workflow.getMemoryMb();
            int perContainerMemoryMb = totalMemoryMb / stage.getTasks().size();

            HashMap<String, String> stageVariables = new HashMap<>(variables);
            stage.getVariables().forEach(stageVariables::putIfAbsent);

            for (WorkflowTask task : stage.getTasks()) {
                HashMap<String, String> taskVariables = new HashMap<>(stageVariables);
                task.getVariables().forEach(taskVariables::putIfAbsent);

                int containerMemoryLimit = task.getMemoryMb() != 0 ? task.getMemoryMb() : perContainerMemoryMb;

                if (containerMemoryLimit < 6) {
                    containerMemoryLimit = 6;
                }

                pipelineStage.getContainers().add(new PipelineContainer(task, taskVariables, containerMemoryLimit));
            }
            data.getStages().add(pipelineStage);
        }
    }

    public Set<String> getImages() {
        return workflow.getStages().stream()
                .flatMap(s -> s.getTasks().stream())
                .map(WorkflowTask::getImage)
                .collect(Collectors.toSet());
    }

    public Set<String> getHostMounts() {
        return workflow.getStages().stream()
                .flatMap(s -> s.getTasks().stream())
                .flatMap(task -> Arrays.stream(task.getHostMounts()))
                .map(mount -> mount.substring(0, mount.indexOf(":")))
                .collect(Collectors.toSet());
    }
}
