package fr.epita.cri.maas.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.Proxy;
import java.net.URL;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class FileUtils {
    private static final Logger logger = LoggerFactory.getLogger(FileUtils.class);

    public static void downloadFile(String inputFileUrl, File inputFile) {
        downloadFile(inputFileUrl, inputFile, new HashMap<>());
    }

    public static void downloadFile(String inputFileUrl, File inputFile, Map<String, String> additionalHeaders) {
        downloadFile(inputFileUrl, inputFile, additionalHeaders, Proxy.NO_PROXY);
    }

    public static boolean downloadFile(String inputFileUrl, File inputFile, Map<String, String> additionalHeaders, Proxy proxy) {
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(inputFileUrl).openConnection(proxy);
            additionalHeaders.forEach(httpURLConnection::setRequestProperty);
            int responseCode = httpURLConnection.getResponseCode();
            if (responseCode >= 400) {
                logger.error("Http response code {} while downloading {}", responseCode, inputFileUrl);
                return false;
            }
            try (InputStream is = httpURLConnection.getInputStream();
                 FileOutputStream fos = new FileOutputStream(inputFile)) {
                byte[] buffer = new byte[1024];
                int readedLen;
                while ((readedLen = is.read(buffer)) >= 0) {
                    fos.write(buffer, 0, readedLen);
                }
            }
            return true;
        } catch (IOException e) {
            logger.error("Exception while trying to download file", e);
        }
        return false;
    }

    public static void deleteDir(File file) {
        if (!Files.isSymbolicLink(file.toPath()) && file.isDirectory()) {
            for (File f : file.listFiles()) {
                deleteDir(f);
            }
        }
        file.delete();
    }

    public static void unzipFile(File zipFile, File outputDirectory) {
        try (ZipInputStream zis = new ZipInputStream(new FileInputStream(zipFile))) {
            ZipEntry zipEntry;
            byte[] buff = new byte[1024];
            int readedLen;
            while ((zipEntry = zis.getNextEntry()) != null) {
                File outputFile = new File(outputDirectory, zipEntry.getName());

                File parent = outputFile.getParentFile();
                if (!parent.exists()) {
                    parent.mkdirs();
                }

                try (FileOutputStream fos = new FileOutputStream(outputFile)) {
                    while ((readedLen = zis.read(buff)) >= 0) {
                        fos.write(buff, 0, readedLen);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static boolean isPathEscaping(String path) {
        if (path.startsWith("/")) {
            return true;
        }
        File parent = new File(".");
        return !isParentOrSame(parent, new File(parent, path));
    }

    public static boolean isParentOrSame(File parent, File file) {
        try {
            parent = parent.getCanonicalFile();
            file = file.getCanonicalFile();
            if (file.equals(parent)) {
                return true;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return isParent(parent, file);
    }

    public static boolean isParent(File parent, File file) {
        try {
            parent = parent.getCanonicalFile();
            file = file.getCanonicalFile();
            return isParentSimple(parent, file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean isParentSimple(File parent, File file) {
        file = file.getParentFile();
        while (file != null) {
            if (file.equals(parent)) {
                return true;
            }
            file = file.getParentFile();
        }
        return false;
    }
}
