package fr.epita.cri.maas.data.workflow;

import fr.epita.cri.maas.util.FileUtils;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class WorkflowStage {
    private String name;
    private List<WorkflowTask> tasks;
    private HashMap<String, String> variables = new HashMap<>();

    public void sanitize(Workflow workflow) throws IllegalArgumentException {
        if (variables == null) {
            variables = new HashMap<>();
        }

        if (tasks == null || tasks.size() == 0) {
            throw new IllegalArgumentException("A stage must have at least one task (Error on: " + name + ")");
        }

        for (WorkflowTask task : tasks) {
            task.sanitize(workflow);
        }

        File parentFile = new File(".");
        Set<File> mountsHostPath = tasks.stream()
                .flatMap(task -> Arrays.stream(task.getMounts()))
                .map(m -> m.substring(0, m.indexOf(':')))
                .map(p -> new File(parentFile, p))
                .map(p -> {
                    try {
                        return p.getCanonicalFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return null;
                })
                .collect(Collectors.toSet());


        for (File hp1 : mountsHostPath) {
            for (File hp2 : mountsHostPath) {
                if (FileUtils.isParentSimple(hp1, hp2)) {
                    int beginIndex = parentFile.getAbsolutePath().length() - 2;
                    //If we let that happen, a user might try to substitute a symlink just at the right time
                    throw new IllegalArgumentException("Can't have a task mounting the parent of an other task mount in the same stage (" + hp1.getPath().substring(beginIndex) + "/ and " + hp2.getPath().substring(beginIndex) + "/)");
                }
            }
        }
    }
}
