package fr.epita.cri.maas.websocket;

import com.fasterxml.jackson.databind.JsonNode;
import fr.epita.cri.maas.message.worker.WorkerMessage;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class WebsocketMessage {
    private Class<? extends WorkerMessage> type;
    private JsonNode data;
}
