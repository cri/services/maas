package fr.epita.cri.maas.data.pipeline;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;

@Getter
@NoArgsConstructor
public class PipelineData {
    private ArrayList<PipelineStage> stages = new ArrayList<>();
    @Setter
    private Collection<Integer> cpuCores;
    @Setter
    private PipelineStatus status = PipelineStatus.WAITING;
    @Setter
    private String resultDataUrl;
    @Setter
    private Instant lastStatusUpdate = Instant.now();

    public PipelineData(Instant lastStatusUpdate, PipelineStatus status) {
        this.lastStatusUpdate = lastStatusUpdate;
        this.status = status;
    }

    public PipelineData(Instant lastStatusUpdate, PipelineStatus status, String resultDataUrl) {
        this.lastStatusUpdate = lastStatusUpdate;
        this.status = status;
        this.resultDataUrl = resultDataUrl;
    }
}
