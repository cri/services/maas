package fr.epita.cri.maas.data.http;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class GetUserTokenResponse {
    private UUID token;
    private List<String> tenants;
}
