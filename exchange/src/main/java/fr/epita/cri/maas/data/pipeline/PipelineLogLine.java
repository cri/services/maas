package fr.epita.cri.maas.data.pipeline;

import fr.epita.cri.maas.util.TermCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.UUID;

@AllArgsConstructor
@Getter
public class PipelineLogLine {
    private UUID containerUuid;
    private String logLine;

    @Override
    public String toString() {
        int l = Math.abs((int) containerUuid.getLeastSignificantBits());
        TermCode[] colors = TermCode.getVibrantColors();
        l %= colors.length;
        TermCode color = colors[l];
        return color.toString() + containerUuid + TermCode.COLOR_DARK_GRAY + " | " + TermCode.RESET + logLine;
    }
}
