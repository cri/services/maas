package fr.epita.cri.maas.util;

import lombok.Getter;

import java.awt.*;

@Getter
public class TermCode {
    public static final TermCode BOLD = new TermCode("1", null);
    public static final TermCode DIM = new TermCode("2", null);
    public static final TermCode UNDERLINED = new TermCode("4", null);
    public static final TermCode BLINK = new TermCode("5", null);
    public static final TermCode INVERTED = new TermCode("7", null);
    public static final TermCode HIDDEN = new TermCode("8", null);

    public static final TermCode RESET = new TermCode("0", null);
    public static final TermCode RESET_BOLD = new TermCode("21", null);
    public static final TermCode RESET_DIM = new TermCode("22", null);
    public static final TermCode RESET_UNDERLINED = new TermCode("24", null);
    public static final TermCode RESET_BLINK = new TermCode("25", null);
    public static final TermCode RESET_REVERSE = new TermCode("27", null);
    public static final TermCode RESET_HIDDEN = new TermCode("28", null);

    public static final TermCode COLOR_DEFAULT = new TermCode("39", new Color(0xFFFFFF));
    public static final TermCode COLOR_BLACK = new TermCode("30", new Color(0x000000));
    public static final TermCode COLOR_RED = new TermCode("31", new Color(0x870000));
    public static final TermCode COLOR_GREEN = new TermCode("32", new Color(0x2E9700));
    public static final TermCode COLOR_YELLOW = new TermCode("33", new Color(0xA07F00));
    public static final TermCode COLOR_BLUE = new TermCode("34", new Color(0x005F9E));
    public static final TermCode COLOR_MAGENTA = new TermCode("35", new Color(0x6700B3));
    public static final TermCode COLOR_CYAN = new TermCode("36", new Color(0x00AD87));
    public static final TermCode COLOR_LIGHT_GRAY = new TermCode("37", new Color(0x9A9A9A));
    public static final TermCode COLOR_DARK_GRAY = new TermCode("90", new Color(0x474747));
    public static final TermCode COLOR_LIGHT_RED = new TermCode("91", new Color(0xFF0000));
    public static final TermCode COLOR_LIGHT_GREEN = new TermCode("92", new Color(0x53FF00));
    public static final TermCode COLOR_LIGHT_YELLOW = new TermCode("93", new Color(0xFFCA00));
    public static final TermCode COLOR_LIGHT_BLUE = new TermCode("94", new Color(0x0098FF));
    public static final TermCode COLOR_LIGHT_MAGENTA = new TermCode("95", new Color(0x9200FF));
    public static final TermCode COLOR_LIGHT_CYAN = new TermCode("96", new Color(0x00FFC7));
    public static final TermCode COLOR_WHITE = new TermCode("97", new Color(0xFFFFFF));

    public static final TermCode COLOR_BACKGROUND_DEFAULT = new TermCode("49", new Color(0xFFFFFF));
    public static final TermCode COLOR_BACKGROUND_BLACK = new TermCode("40", new Color(0x000000));
    public static final TermCode COLOR_BACKGROUND_RED = new TermCode("41", new Color(0x870000));
    public static final TermCode COLOR_BACKGROUND_GREEN = new TermCode("42", new Color(0x2E9700));
    public static final TermCode COLOR_BACKGROUND_YELLOW = new TermCode("43", new Color(0xA07F00));
    public static final TermCode COLOR_BACKGROUND_BLUE = new TermCode("44", new Color(0x005F9E));
    public static final TermCode COLOR_BACKGROUND_MAGENTA = new TermCode("45", new Color(0x6700B3));
    public static final TermCode COLOR_BACKGROUND_CYAN = new TermCode("46", new Color(0x00AD87));
    public static final TermCode COLOR_BACKGROUND_LIGHT_GRAY = new TermCode("47", new Color(0x9A9A9A));
    public static final TermCode COLOR_BACKGROUND_DARK_GRAY = new TermCode("100", new Color(0x474747));
    public static final TermCode COLOR_BACKGROUND_LIGHT_RED = new TermCode("101", new Color(0xFF0000));
    public static final TermCode COLOR_BACKGROUND_LIGHT_GREEN = new TermCode("102", new Color(0x53FF00));
    public static final TermCode COLOR_BACKGROUND_LIGHT_YELLOW = new TermCode("103", new Color(0xFFCA00));
    public static final TermCode COLOR_BACKGROUND_LIGHT_BLUE = new TermCode("104", new Color(0x0098FF));
    public static final TermCode COLOR_BACKGROUND_LIGHT_MAGENTA = new TermCode("105", new Color(0x9200FF));
    public static final TermCode COLOR_BACKGROUND_LIGHT_CYAN = new TermCode("106", new Color(0x00FFC7));
    public static final TermCode COLOR_BACKGROUND_WHITE = new TermCode("107", new Color(0xFFFFFF));

    @Getter
    private static final TermCode[] colors = new TermCode[]{
            COLOR_DEFAULT,
            COLOR_BLACK,
            COLOR_RED,
            COLOR_GREEN,
            COLOR_YELLOW,
            COLOR_BLUE,
            COLOR_MAGENTA,
            COLOR_CYAN,
            COLOR_LIGHT_GRAY,
            COLOR_DARK_GRAY,
            COLOR_LIGHT_RED,
            COLOR_LIGHT_GREEN,
            COLOR_LIGHT_YELLOW,
            COLOR_LIGHT_BLUE,
            COLOR_LIGHT_MAGENTA,
            COLOR_LIGHT_CYAN,
            COLOR_WHITE,
    };

    @Getter
    private static final TermCode[] vibrantColors = new TermCode[]{
            COLOR_RED,
            COLOR_GREEN,
            COLOR_YELLOW,
            COLOR_BLUE,
            COLOR_MAGENTA,
            COLOR_CYAN,
            COLOR_LIGHT_RED,
            COLOR_LIGHT_GREEN,
            COLOR_LIGHT_YELLOW,
            COLOR_LIGHT_BLUE,
            COLOR_LIGHT_MAGENTA,
            COLOR_LIGHT_CYAN,
    };

    private final String code;
    private final int color;

    private TermCode(String code) {
        this(code, null);
    }

    private TermCode(String code, Color color) {
        this.code = "\u001b[" + code + "m";
        if (color != null) {
            this.color = color.getRGB();
        } else {
            this.color = -1;
        }
    }

    @Override
    public String toString() {
        return code;
    }
}
