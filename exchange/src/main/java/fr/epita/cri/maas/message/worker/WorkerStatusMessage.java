package fr.epita.cri.maas.message.worker;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class WorkerStatusMessage extends WorkerUpstreamMessage {
    private int cpuCount;
    private int memoryMb;
    private List<UUID> runningPipelines;
}
