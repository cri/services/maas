package fr.epita.cri.maas.websocket;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.epita.cri.maas.message.worker.WorkerMessage;
import fr.epita.cri.maas.util.SerializerUtils;

public class WebsocketSerializer {
    private static final ObjectMapper websocketObjectMapper = SerializerUtils.getJsonObjectMapper();

    public static String serializeWebsocketMessage(WorkerMessage workerMessage) {
        JsonNode data = websocketObjectMapper.valueToTree(workerMessage);
        try {
            return websocketObjectMapper.writeValueAsString(new WebsocketMessage(workerMessage.getClass(), data));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static WorkerMessage deserializeWebsocketMessage(String s) {
        try {
            WebsocketMessage websocketMessage = websocketObjectMapper.readValue(s, WebsocketMessage.class);
            WorkerMessage workerMessage = websocketObjectMapper.treeToValue(websocketMessage.getData(), websocketMessage.getType());
            return workerMessage;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }
}
