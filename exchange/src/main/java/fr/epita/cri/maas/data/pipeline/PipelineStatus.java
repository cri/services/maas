package fr.epita.cri.maas.data.pipeline;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum PipelineStatus {
    WAITING(false, "W"),
    SCHEDULED(false, "S"),
    SETUP(false, "ST"),
    RUNNING(false, "R"),
    INTERRUPTED(false, "I"),
    CANCELLED(true, "C"),
    FAILED(true, "F"),
    ENDED(true, "E");

    private boolean done;
    private String shortname;
}
