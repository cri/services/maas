package fr.epita.cri.maas.message.worker;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class WorkerAddPipelineContainerLogMessage extends WorkerUpstreamMessage {
    private UUID pipelineUuid;
    private UUID containerUuid;
    private String logLine;
}
