package fr.epita.cri.maas.data.workflow;

import fr.epita.cri.maas.util.FileUtils;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class WorkflowTask {
    private String name;
    private String image;
    private String inputFilePath;
    private String outputFilePath;
    private int timeout = 60;//Default timeout of 60s
    private int cpus = 1;
    private String[] mounts = new String[]{};
    private String[] hostMounts = new String[]{};
    private int memoryMb;
    private String[] capabilities = new String[]{};
    private String commandInterpreter = "sh";
    private String[] command;
    private String[] commands;
    private HashMap<String, String> variables = new HashMap<>();
    private boolean interruptOnFailure;
    private String workdir;

    public void sanitize(Workflow workflow) throws IllegalArgumentException {
        if (image != null && !image.matches("^[A-Za-z0-9_/.-]+:[A-Za-z0-9_/.-]+$")) {
            throw new IllegalArgumentException("Invalid image");
        }
        if (inputFilePath != null && !inputFilePath.matches("^[A-Za-z0-9_/.-]+$")) {
            throw new IllegalArgumentException("Invalid input file path");
        }
        if (outputFilePath != null && !outputFilePath.matches("^[A-Za-z0-9_/.-]+$")) {
            throw new IllegalArgumentException("Invalid output file path");
        }
        if (timeout < 1) {
            throw new IllegalArgumentException("Invalid task timeout");
        }
        if (cpus < 0 || cpus > workflow.getCpuCount()) {
            throw new IllegalArgumentException("Invalid cpus, too much cpus");
        }

        if (mounts == null) {
            mounts = new String[]{};
        }

        for (String mount : mounts) {
            if (!mount.matches("^[A-Za-z0-9_/.-]+:[A-Za-z_0-9/.-]+(:ro|:rw)?$")) {
                throw new IllegalArgumentException("Invalid mount");
            }
            String fromPath = mount.substring(0, mount.indexOf(':'));
            if (FileUtils.isPathEscaping(fromPath)) {
                throw new IllegalArgumentException("Invalid mount path " + fromPath);
            }
        }

        for (String mount : hostMounts) {
            if (!mount.matches("^[A-Za-z0-9_/.-]+:[A-Za-z_0-9/.-]+(:ro|:rw)?$")) {
                throw new IllegalArgumentException("Invalid mount");
            }
        }

        if (memoryMb < 0 || memoryMb > workflow.getMemoryMb()) {
            throw new IllegalArgumentException("Invalid memory");
        }

        if (capabilities == null) {
            capabilities = new String[]{};
        }

        if (variables == null) {
            variables = new HashMap<>();
        }

        HashSet<String> capabilitiesSet = new HashSet<>();
        for (String capability : capabilities) {
            if (!capabilitiesSet.add(capability)) {
                throw new IllegalArgumentException("Duplicate capability " + capability);
            }
            if (!capability.equals("SYS_PTRACE")) {//TODO Global authorized capabilities & per tenant ?
                throw new IllegalArgumentException("Invalid capability");
            }
        }
        if (command != null && commands != null) {
            throw new IllegalArgumentException("You can't use both command and commands at the same time");
        }
        if (workdir != null && !workdir.matches("^[A-Za-z0-9_/.-]+$")) {
            throw new IllegalArgumentException("Invalid workdir");
        }
    }
}
