package fr.epita.cri.maas.data.workflow;

import com.fasterxml.jackson.core.JsonProcessingException;
import fr.epita.cri.maas.util.SerializerUtils;

public class WorkflowDeserializer {
    public static Workflow getWorkflow(String workflowFileContent) {
        if (workflowFileContent == null) {
            return null;
        }
        try {
            return SerializerUtils.getYamlObjectMapper().readValue(workflowFileContent, Workflow.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }
}
