package fr.epita.cri.maas.data.http;

import fr.epita.cri.maas.data.pipeline.PipelineStatus;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class PipelineStatusBody {
    private UUID pipelineId;
    private PipelineStatus pipelineStatus;
}
