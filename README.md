# MaaS

Système de moulinettage multi-tenant polyvalent conçu pour exécuter des tâches
de calcul de manière isolée. Permet de répondre à d'importantes demandes en
puissance de calcul à la demande.

## [Utilisateur] Principe de fonctionnement

Le système fonctionne en exécutant des workflow. Un workflow est une liste
d'instructions à exécuter dans un format similaire aux `.gitlab-ci.yaml`

Un workflow très simple exécute une image Docker récupérant de la donnée,
exécutant une tâche de calcul avec puis retournant un résultat.

Le système dispose de limites nécessaires à son bon fonctionnement qui sont:

* Les conteneurs n'ont pas d'accès à internet
* Les conteneurs ne sont pas privilégiés

### Exemple de workflow

Voici un exemple de workflow:

```yaml
---

name: cosmetic_name
variables:
 KEY: value
stages:
 - tasks:
     - image: ubuntu
       input_file_path: /data/in
       output_file_path: /data/out
       commands:
         - sleep 90s
timeout: 10
```

Le nom `name: cosmetic_name` est purement cosmétique et permet simplement de s'y
retrouver dans la liste des pipelines lancées.

#### Variables

Les variables sont ici définies de manière globales, ce sont des variables
d'environement qui sont transmise aux conteneurs. Il est également possible de
les spécifier au niveau d'une tâche.

Le véritable intérêt des variables est de les fournir au moment de la demande de
pipeline. Cela permet d'avoir certaines parties dynamiques sans devoir templater
le workflow.

#### Stages & Tasks

Les `stages` sont un ensembles d'étapes qui sont exécutées les unes après les
autres. Chaque étape est constitué de `tasks` qui sont chacunes un conteneur à
exécuter. Les tâches d'une étapes sont, dans la mesure du possible, exécutés
simultanément et permettent de paralléliser certaines actions.

Chaque tâche représente un conteneur docker qui est lancé. La valeur `image` est
l'adresse de l'image à exécuter.

#### Entrée & Sortie

Les chemins `input_file_path` et `output_file_path` permettent de définir le
point de montage du fichier d'entré et le fichier de sortie dans le conteneur.

Plusieurs conteneurs peuvent monter le fichier d'entré et le fichier de sortie,
ou alors uniquement l'un des deux.

Les fichiers d'entrée et de sortie n'ont pas de format défini, il incombe au
créateur du workflow de le définir, simplement par l'utilisation qu'il en a. Par
exemple, le fichier d'entré peut être une tarball, qui sera décompressée par le
premier conteneur et le format de sortie un xml.

#### Commandes

Peuvent ensuite être précisés un ensemble de `commands:` qui seront exécutés
dans le conteneur. Ces commandes sont placés dans un fichier monté dans le
conteneur puis exécuté avec la commande `bash -c ./commands`. L'interpréteur
peut être modifié en utilisant la propriété `command_interpreter:`.

Dans le cas ou aucune commande n'est spécifié le conteneur est lancé sans et
c'est donc la commande précisée dans le Dockerfile au moment de sa construction
qui est exécutée.

#### Cpus

Il est possible de pin un conteneur sur un ou plusieurs cpu. Dans le cas ou
plusieurs conteneur sont exécutés simultanément comportant un conteneur par
projet d'élève et un conteneur central d'arbitrage il est possible d'éviter que
du code peu fiable monopolise les resources CPU & mémoire alloués au workflow.

Cela se fait en sélectionnant manuellement les CPU alloués à chaque conteneur,
par le biais de la propriété `cpus:`. Par exemple `cpus: [0, 3]` pin le
conteneur sur le premier et le 4eme coeur CPU alloués au job.

On note que ce sont bien des coeurs CPU qui sont pin et non des threads.
Cependant, leur performance ne sont pas garanties en raison de l'hébergement sur
des VM des workers.

#### Mémoire

Par défaut, les conteneurs se divisent la mémoire alloué à un job de manière
équitable, si une tâche a des besoins particuliers, la propriété `memory_mb:`
peut être utilisés.

#### Points de montage

Pour transférer des donnés entre les conteneurs, il est possible d'utiliser des
points de montage. Cela permet de partager des répertoires qui seront
accessibles en lecture et écriture à tous les conteneurs d'un job qui les
montent. Cela se fait par l'utilisation de la propriété `mounts:` qui s'utilise
de la manière suivante:

```yaml
stages:
 - tasks:
     - image: img1
       mounts:
         - "/data:/data"
     - image: img2
       mounts:
         - "/data/code:/code"
```

Dans cet exemple, tout ce qui est placé dans le dossier `/data/code` dans le
premier conteneur se retrouve dans le dossier `/code` dans le second.

#### Capabilities

Les conteneurs ne sont pas privilégiés mais il est parfois nécessaire de
disposer de certaines permissions. Il est donc possible de demander des
capabilities avec `capabilities:`.

Ces capacités, en raison du risque qu'elles représentent, sont filtrés et
uniquement quelques-unes sont autorisées.

#### Timeout

Il est également possible de préciser un timeout par conteneur et un timeout
global. Cela se fait par l'utilisation de la propriété `timeout:` puis la valeur
du timeout en secondes à placer au niveau du job ou d'une task.

## Organisation du projet

Worker: Le logiciel à déployer sur les workers, se connecte au scheduler pour
récupérer des pipelines

Scheduler: Cerveau de l'unité, réceptionne, stocke eet distribue les tâches

CLI: Outil permettant de tester les workflow en local ou les envoyer au
scheduler

Runner: Code permettant l'exécution d'une tâche sur la machine locale

Exchange: Ensemble des dépendances utilisés pour la communication entre les
services

```mermaid
graph TD;
 Worker-->Runner;
 Scheduler-->Exchange;
 Runner-->Exchange;
 CLI-->Runner;
```

## Principes d'opération

Le scheduler est conçu pour pouvoir fonctionner sur un kube. Il expose une API
Rest HTTP qui permet de lancer des workflow et de récupérer leurs informations
mais il est réellement conçu pour utiliser un Kafka pour la transmission des
informations.

Cela veut dire que les workflows envoyés pendant une indisponibilité sont
récupérés au lancement du service, contrairement aux requêtes HTTP qui peuvent
se perdre.

Le scheduler expose également un serveur websocket qui permet une communication
avec l'ensemble des workers.

Les workers transmettent leur état au scheduler, qui va ensuite utiliser cette
information pour exécuter son algorithme de scheduling qui va générer un
ensemble d'ordres qui vont venir faire modifier leur état aux workers.

```mermaid
graph TD;
Worker-->State;
Scheduler-->Worker;
State-->Scheduler;
```

Cela permet donc l'implémentation d'un système tolérant aux pannes relativement
simplement. Le state d'un worker indisponible est supprimé, causant
naturellement un nouveau scheduling des tâches dont il est responsable et
l'arrivée d'un nouveau qui annonce son état permet, dans la mesure du possible,
de récupérer l'état d'avancée de ses pipelines.

Il est donc possible de redémarrer ou reconnecter les différents éléments sans
ordre préétablis sans interrompre les tâches qui tournent ou nécessiter un
redémarrage global du système.
